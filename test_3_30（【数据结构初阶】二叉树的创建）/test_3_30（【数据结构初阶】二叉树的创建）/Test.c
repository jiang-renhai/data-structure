#define _CRT_SECURE_NO_WARNINGS 1

//递归
#include"BinaryTreeNode.h"

typedef int BTDataType;
typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;


BTNode* BuyNode(BTDataType x)
{
	BTNode* BTnode = (BTNode*)malloc(sizeof(BTNode));
	if (BTnode == NULL)
	{
		perror("malloc fail");
		return;
	}

	BTnode->data = x;
	BTnode->left = NULL;
	BTnode->right = NULL;

	return BTnode;
}

BTNode* CreatBinaryTree()
{
	BTNode* node1 = BuyNode(1);
	BTNode* node2 = BuyNode(2);
	BTNode* node3 = BuyNode(3);
	BTNode* node4 = BuyNode(4);
	BTNode* node5 = BuyNode(5);
	BTNode* node6 = BuyNode(6);
	BTNode* node7 = BuyNode(7);

	node1->left = node2;
	node1->right = node4;

	node2->left = node3;
	node2->right = node7;

	node4->left = node5;
	node4->right = node6;

	//node3->right = node7;

	return node1;
}

//前序 -- 根、左子树、右子树
void PreOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	printf("%d ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}

//中序 -- 左子树、根、右子树
void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

//后序 -- 左子树、右子树、根
void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

//算结点个数 -- 指挥打工人统计数量报上来
int TreeSize(BTNode* root)
{
	//指挥打工人
	return root == NULL ? 0 : 
			TreeSize(root->left)
			+ TreeSize(root->right) 
			+ 1;
}

//当前树的高度 == 左右子树高的那个层数+1
int TreeHeight(BTNode* root)
{
	if (root == NULL)
		return 0;
	int LeftTreeHeight = TreeHeight(root->left);
	int RightTreeHeight = TreeHeight(root->right);

	return LeftTreeHeight > RightTreeHeight ? 
			LeftTreeHeight + 1 
			: RightTreeHeight + 1;
}

//二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	assert(k > 0);
	//当前树的第k层的个数 = 左子树的第k-1层个数 + 左子树的第k-1层个数
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;
	int LeftK = BinaryTreeLevelKSize(root->left, k - 1);
	int RightK = BinaryTreeLevelKSize(root->right, k - 1);
	return LeftK + RightK;
}

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
		return NULL;
	if (root->data == x)
		return root;
	BTNode* lret = BinaryTreeFind(root->left, x);
	if (lret)
		return lret;
	BTNode* rret = BinaryTreeFind(root->right, x);
	if (rret)
		return rret;
}

//层序遍历
void LevelOrder(BTNode* root)
{
	//先创建一个队列
	Queue q;
	QueueInit(&q);
	//根结点不为空则插入到队列当中
	if (root)
		QueuePush(&q, root);
	//往后遍历
	//出一个头结点进下一层结点
	while (!QueueEmpty(&q))
	{
		//拿出队头结点
		BTNode* front = QueueFront(&q);
		//把队列中的pop掉这样不会导致树里面的数据丢失
		QueuePop(&q);
		printf("%d ", front->data);

		//push左树和右树
		if (front->left)
			QueuePush(&q, front->left);
		if (front->right)
			QueuePush(&q, front->right);

	}
	printf("\n");

	QueueDestroy(&q);
}

// 二叉树销毁
void BinaryTreeDestory(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreeDestory(root->left);
	BinaryTreeDestory(root->right);
	free(root);
}


// 判断二叉树是否是完全二叉树
bool BinaryTreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	//根结点不为空则插入到队列当中
	if (root)
		QueuePush(&q, root);
	//往后走，到NULL则退出，取完数据
	while (!QueueEmpty(&q))
	{
		//拿出一个队头结点
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		if (front == NULL)
		{
			break;
		}
		else
		{
			QueuePush(&q, front->left);
			QueuePush(&q, front->right);
		}
	}
	//判断是否为完全二叉树
	while (!QueueEmpty(&q))
	{
		//取队头数据
		BTNode* front1 = QueueFront(&q);
		QueuePop(&q);

		//判断队头数据是否为空，为空则继续走,不为空则输出false
		if (front1)
		{
			QueueDestroy(&q);
			return false;
		}

	}
	QueueDestroy(&q);
	return true;
}


int main()
{
	BTNode* root = CreatBinaryTree();
	PreOrder(root);
	printf("\n");
	InOrder(root);
	printf("\n");
	PostOrder(root);
	printf("\n");

	int size = TreeSize(root);
	printf("TreeSize:%d\n", size);
	int size1 = TreeSize(root);
	printf("TreeSize:%d\n", size1);

	int height = TreeHeight(root);
	printf("HeightSize:%d\n", height);

	int K1 = BinaryTreeLevelKSize(root, 3);//层数
	printf("BinaryTreeLevelKSize:%d\n", K1);

	int K2 = BinaryTreeLevelKSize(root, 2);//层数
	printf("BinaryTreeLevelKSize:%d\n", K2);

	printf("地址为：%p\n", BinaryTreeFind(root, 4));
	printf("地址为：%p\n", BinaryTreeFind(root, 50));

	printf("LevelOrder:");
	LevelOrder(root);

	printf("判断是否为完全二叉树:");
	printf("%d\n", BinaryTreeComplete(root));

	BinaryTreeDestory(root);

	return 0;
}