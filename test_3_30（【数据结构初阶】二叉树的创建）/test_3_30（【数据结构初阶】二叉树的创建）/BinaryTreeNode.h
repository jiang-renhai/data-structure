#pragma once

//用链表实现
//先进先出
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include<stdbool.h>

typedef struct BinaryTreeNode* QueueDataType;

typedef struct QueueNode {
	struct QueueNode* next;
	QueueDataType data; //结构体指针，指向的是树里面的结点
}QNode;

typedef struct Queue {
	QNode* head;
	QNode* tail;
	int size;
}Queue;

//初始化
void QueueInit(Queue* pq); //改变结构体成员就用结构体的指针即可

//销毁链表
void QueueDestroy(Queue* pq);

//进队列
void QueuePush(Queue* pq, QueueDataType x);

//出队列
void QueuePop(Queue* pq);

//计算长度
int QueueSize(Queue* pq);

//判断是否为空
bool QueueEmpty(Queue* pq);

//队头
QueueDataType QueueFront(Queue* pq);

//队尾
QueueDataType QueueBack(Queue* pq);