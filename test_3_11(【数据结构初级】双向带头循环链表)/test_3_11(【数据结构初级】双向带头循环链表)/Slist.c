#define _CRT_SECURE_NO_WARNINGS 1

#include"Slist.h"

//增加结点
LTNode* BuyListNode(LTDataType x) {
	LTNode* newnode = (LTNode*)malloc(sizeof(LTNode));
	if (newnode == NULL) {
		perror("malloc::newnode");
		return NULL;
	}
	newnode->next = NULL;
	newnode->prev = NULL;
	newnode->data = x;

	return newnode;
}

//初始化
LTNode* LTInit() {
	LTNode* phead = BuyListNode(-1);
	phead->next = phead;
	phead->prev = phead;

	return phead;
}

//判断链表是否为空
bool LTEmpty(LTNode* phead) {
	assert(phead);
	//if (phead->next == phead) {
	//	return true;
	//}
	//else{
	//	return false;
	//}
	return phead->next == phead;//空就是真
}


//打印
void LTPrint(LTNode* phead) {
	assert(phead);
	LTNode* cur = phead->next;
	printf("head<=>");
	while (cur != phead) {
		printf("%d<=>", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

//销毁链表
void LTDestroy(LTNode* phead) {
	assert(phead);
	LTNode* cur = phead->next;
	LTNode* next = cur->next;
	while (cur != phead) {
		free(cur);
		cur = next;
		next = next->next;
	}
	free(phead);
	phead = NULL;
}

//尾插
void LTPushBack(LTNode* phead, LTDataType x) {
	assert(phead);

	//不管链表为空还是不为空都可以插入
	LTNode* newnode = BuyListNode(x);
	LTNode* tail = phead->prev;

	tail->next = newnode;
	newnode->prev = tail;
	newnode->next = phead;
	phead->prev = newnode;
}

//尾删
void LTPopBack(LTNode* phead) {
	assert(phead);
	assert(!LTEmpty(phead));

	LTNode* tail = phead->prev;
	LTNode* tailprev = tail->prev;

	tailprev->next = phead;
	phead->prev = tailprev;
	free(tail);
	tail = NULL;
	//LTErase(phead->prev);
}

//头插
void LTPushFront(LTNode* phead, LTDataType x) {
	assert(phead);

	LTNode* newnode = BuyListNode(x);
	newnode->next = phead->next;
	phead->next->prev = newnode;

	phead->next = newnode;
	newnode->prev = phead;
}

//头删
void LTPopFront(LTNode* phead) {
	assert(phead);
	assert(!LTEmpty(phead));

	LTNode* first = phead->next;
	phead->next = first->next;
	first->next->prev = phead;
	free(first);
	first = NULL;

	//LTErase(phead->next);
}

//指定前插
void LTInsert(LTNode* pos, LTDataType x) {
	assert(pos);
	LTNode* prev = pos->prev;
	LTNode* newnode = BuyListNode(x);
	//prev  newnode  pos
	prev->next = newnode;
	newnode->prev = prev;

	newnode->next = pos;
	pos->prev = newnode;
}

//指定删除
void LTErase(LTNode* pos) {
	assert(pos);
	LTNode* prev = pos->prev;
	LTNode* later = pos->next;
	//prev pos later
	prev->next = later;
	later->prev = prev;
	free(pos);
	pos = NULL;
}

//查找
LTNode* Find(LTNode* phead, LTDataType x) {
	assert(phead);
	LTNode* cur = phead->next;
	while (cur != phead) {
		if (cur->data == x) {
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}