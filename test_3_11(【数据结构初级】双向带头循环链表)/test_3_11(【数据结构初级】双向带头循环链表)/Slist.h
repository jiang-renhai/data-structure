#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

typedef int LTDataType;

typedef struct ListNode 
{
	struct ListNode* next;//后
	struct ListNode* prev;//前

	LTDataType data;//数据
}LTNode;

//初始化
LTNode* LTInit();

//尾插
void LTPushBack(LTNode* phead, LTDataType x);

//尾删
void LTPopBack(LTNode* phead);

//销毁链表
void LTDestroy(LTNode* phead);

//打印
void LTPrint(LTNode* phead);

//判断链表是否为空
bool LTEmpty(LTNode* phead);

//头插
void LTPushFront(LTNode* phead, LTDataType x);

//头删
void LTPopFront(LTNode* phead);

//指定前插
void LTInsert(LTNode* pos, LTDataType x);

//指定删除
void LTErase(LTNode* pos);

//查找
LTNode* Find(LTNode* phead, LTDataType x);