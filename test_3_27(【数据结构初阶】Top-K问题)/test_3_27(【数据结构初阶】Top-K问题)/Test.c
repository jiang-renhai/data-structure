#define _CRT_SECURE_NO_WARNINGS 1
#include"TopK.h"


void PrintfTopK(const char* file, int k)
{
	//建堆，用a中前k个数建堆
	int* TopK = (int*)malloc(sizeof(int) * k);
	if (TopK == NULL)
	{
		perror("malloc TopK");
		return;
	}

	FILE* fout = fopen(file, "r");
	if (fout == NULL)
	{
		perror("fopen file");
		return;
	}

	//读出前k个元素
	for (int i = 0; i < k; i++)
	{
		fscanf(fout, "%d", &TopK[i]);
	}
	//进行建堆
	for (int j = (k - 2) / 2; j >= 0; j--)
	{
		AdjustDown1(TopK, k, j);
	}

	//剩余n-k个元素依次与堆顶元素交换，不满则替换
	int val = 0;
	int ret = fscanf(fout, "%d", &val);
	while (ret != EOF)
	{
		if (val > TopK[0])
		{
			TopK[0] = val;
			AdjustDown1(TopK, k, 0);
		}
		ret = fscanf(fout, "%d", &val);
	}

	//打印
	for (int i = 0; i < k; i++)
	{
		printf("%d ", TopK[i]);
	}
	fclose(fout);
	fout = NULL;

}

void CreatNData()
{
	//TopK问题
	int n = 10000;
	srand(time(0));
	const char* file = "data.txt";
	FILE* fin = fopen(file, "w");
	if (fin == NULL)
	{
		perror("fopen file");
		return;
	}
	for (int i = 0; i < n; i++)
	{
		int x = rand() % 10000;
		fprintf(fin, "%d\n", x);
	}
	fclose(fin);
	fin = NULL;
}


int main()
{
	//CreatNData();
	PrintfTopK("data.txt", 10);

	return 0;
}