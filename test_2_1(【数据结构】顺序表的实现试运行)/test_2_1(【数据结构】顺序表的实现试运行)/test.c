#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"

////测试1
//void TestSeqList1()
//{
//	SL s1;
//	SeqListInit(&s1);
//	SeqListPushBack(&s1, 1);
//	SeqListPushBack(&s1, 2);
//	SeqListPushBack(&s1, 3);
//	SeqListPushBack(&s1, 4);
//	SeqListPushBack(&s1, 5);
//
//	SeqListPrint(&s1);
//	SeqListPopBack(&s1);
//	SeqListPopBack(&s1);
//	SeqListPopBack(&s1);
//	//SeqListPopBack(&s1);
//	//SeqListPopBack(&s1);
//	//SeqListPopBack(&s1);
//	SeqListPrint(&s1);
//
//	SeqListDestory(&s1);
//}
//
////测试2
//TestSeqList2() {
//	SL s1;
//	SeqListInit(&s1);
//	SeqListPushBack(&s1, 1);
//	SeqListPushBack(&s1, 2);
//	SeqListPushBack(&s1, 3);
//	SeqListPushBack(&s1, 4);
//	SeqListPushBack(&s1, 5);
//	SeqListPrint(&s1);
//
//	SeqListPushFront(&s1, 10);
//	SeqListPushFront(&s1, 20);
//	SeqListPushFront(&s1, 30);
//	SeqListPrint(&s1);
//
//	SeqListPopFront(&s1);
//	SeqListPopFront(&s1);
//	SeqListPopFront(&s1);
//	SeqListPopFront(&s1);
//	SeqListPrint(&s1);
//
//	SeqListDestory(&s1);
//
//}
//
////测试3
//TestSeqList3() {
//	SL s1;
//	SeqListInit(&s1);
//	SeqListPushBack(&s1, 1);
//	SeqListPushBack(&s1, 2);
//	SeqListPushBack(&s1, 3);
//	SeqListPushBack(&s1, 4);
//	SeqListPushBack(&s1, 5);
//	SeqListPrint(&s1);
//
//	SeqListDestory(&s1);
//
//}
////测试4
//void TestSeqList4() {
//	SL s1;
//	SeqListInit(&s1);
//	SeqListPushBack(&s1, 1);
//	SeqListPushBack(&s1, 2);
//	SeqListPushBack(&s1, 3);
//	SeqListPushBack(&s1, 4);
//	SeqListPushBack(&s1, 5);
//	SeqListPrint(&s1);
//
//	SeqListInsert(&s1, 2, 30);
//	SeqListPrint(&s1);
//	int ret = SeqListFind(&s1, 4);
//	printf("%d\n", ret);
//
//	SeqListErase(&s1, 0);
//	SeqListErase(&s1, 1);
//	SeqListPrint(&s1);
//
//	SeqListDestory(&s1);
//
//}

void menu() {
	printf("\t\t\t**********************\t\t\t\n");
	printf("\t\t\t****请选择你的操作****\t\t\t\n");
	printf("\t\t\t*****1.头插2.头删*****\t\t\t\n");
	printf("\t\t\t*****3.尾插4.尾删*****\t\t\t\n");
	printf("\t\t\t*****5.查找6.打印*****\t\t\t\n");
	printf("\t\t\t*****7.指插8.指删*****\t\t\t\n");
	printf("\t\t\t*****9.排序10.清空****\t\t\t\n");
	printf("\t\t\t*********0.退出*******\t\t\t\n");
}

enum SeqListTest {
	Exit,
	PushFront,
	PopFront,
	PushBack,
	PopBack,
	Find,
	Print,
	Insert,
	Erase,
	Sort,
	Empty
};

void MenuTest() {
	SL s1;
	SeqListInit(&s1);//初始化
	int input = 0;
	do {
		menu();
		int x = 0;
		int pos = 0;
		scanf("%d", &input);
		switch (input) {
		case Exit:
			printf("程序退出\n");
			break;
		case PushFront:
			printf("请输入你要头插的数据,连续输入，以-1结束\n");
			scanf("%d", &x);
			while (x != -1) {
				SeqListPushFront(&s1, x);
				scanf("%d", &x);
			}
			break;
		case PopFront:
			SeqListPopFront(&s1);
			printf("头删一个成功\n");
			break;
		case PushBack:
			printf("请输入你要尾插的数据,连续输入，以-1结束\n");
			scanf("%d", &x);
			while (x != -1) {
				SeqListPushBack(&s1, x);
				scanf("%d", &x);
			}
			break;
		case PopBack:
			SeqListPopBack(&s1);
			printf("尾删一个成功\n");
			break;
		case Find:
			printf("请输入需要查找的元素\n");
			scanf("%d", &x);
			pos = SeqListFind(&s1, x);
			printf("要查找的元素下标在：%d\n", pos);
			break;
		case Print:
			SeqListPrint(&s1);
			break;
		case Insert:
			printf("请输入插入位置和元素大小\n");
			scanf("%d %d", &x, &pos);
			SeqListInsert(&s1, x, pos);
			break;
		case Erase:
			printf("请输入删除的位置\n");
			scanf("%d", &x);
			SeqListErase(&s1, x);
			printf("删除成功\n");
			break;
		case Sort:
			qsort(s1.a, s1.size, sizeof(s1.a[0]), SeqListSortName);
			printf("排序成功\n");
			break;
		case Empty:
			SeqListEmpty(&s1);
			printf("清空成功\n");
			break;
		default:
			printf("请重新输入\n");
			break;
		}
	} while (input);

	SeqListDestory(&s1);
}

int main() {
	//测试
	//TestSeqList1();
	//TestSeqList2();
	//TestSeqList3();
	//TestSeqList4();

	//实现
	MenuTest();
	return 0;
}