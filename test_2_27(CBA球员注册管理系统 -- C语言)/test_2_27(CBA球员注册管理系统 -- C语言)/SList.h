#pragma once
//CBA球员注册管理系统
//0.exit
//1.桌面
//2.增加球员信息（指定插入（尾插））1
//3.删除球员信息（指定删除）1
//4.查找球员信息1
//5.修改成员信息（先找到再修改）1
//6.在指定球员之后增加球员信息（先找到再插入）1
//7.筛选 1
//8.统计 1
//9.排序 1
//10.显示（打印）1
//11.判断是否进入CBA eg：中国人，体脂率10以下，体育成绩优秀，学习成绩优秀1
//12.清空 1
//13.计算（体脂率，学习成绩，体育成绩）1
//14.报表


#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include<windows.h>
#include<errno.h>
#include<conio.h>

#define MAX_ID 20
#define MAX_PWD 30
#define MAX_NAME 20
#define MAX_SEX 2
#define MAX_PHOEN 20

typedef int SLDataType;
typedef char SLDataType1;
typedef float SLDataType2;

//球员个人信息
typedef struct SLPlayer {
	SLDataType1 ID[20];//身份证
	SLDataType1 name[30];//姓名(英文名，如：JiangRenHai)
	SLDataType1 sex[4];//性别(男为m，女为f)
	SLDataType age;//年龄
	SLDataType number;//球衣号码
	SLDataType2 high;//身高(cm)
	SLDataType2 weight;//体重(kg)
	SLDataType2 reach;//臂展(cm)
	SLDataType2 BodyFatPercentage;//体脂率(10.2,11.2)
	SLDataType1 PEResult[15];//运动等级(优良合格)
	SLDataType1 LearingScore[15];//学习等级(优良合格)
}Player;

typedef struct SLCBAPlayer {
	Player cbaplayer;//注册球员的信息
	struct SLCBAPlayer* next;//下一个结点
}SLTCBANode;

//用户注册信息
typedef struct Users {
	char id[MAX_ID];//身份证
	char password[MAX_PWD];//密码
	char name[MAX_NAME];//姓名
	char sex[MAX_SEX];//性别
	char phone[MAX_PHOEN];//电话号码
}users;

//增加球员信息（尾插法）
void InputTailCBAPlayer(SLTCBANode** pphead);

//查找（按照不同方式）
void SListFind(const SLTCBANode* phead);

//打印
void SListPrint(const SLTCBANode* phead);

//修改（修改单人的信息）
void ModifyCBAPlayer(SLTCBANode* phead);

//指插后（先找到再插入）
void InsertCBAPlayerAfter(SLTCBANode** pphead);

//指删（先找到再删除）
void EraseCBAPlayer(SLTCBANode** pphead);

//筛选（根据不同条件进行筛选）
void DressingByScreeningCBAPlayer(const SLTCBANode* phead);

//统计
int StatisticsCBAPlayer(const SLTCBANode* phead);

//销毁链表
void SListDestory(SLTCBANode** pphead);

//排序（根据身份证进行排序）
void BubbleSortCBAPlayer(SLTCBANode** pphead);

//判断是否进入CBA（1.顺利进入）
//（2.未进入（1.不是中国人(办张中国绿卡吧！)）
//(2.体脂率太高3.运动成绩太低4.学习成绩太低)
void JudgeCBAPlayer(SLTCBANode* phead);

//计算（体脂率，学习成绩，体育成绩）
void MathMenu();

//文件的存储
void StorageCBAPlayer(SLTCBANode* phead);

//文件的加载
void LoadingCBAPlayer(SLTCBANode** phead);

//报表
void Excel(const SLTCBANode* phead);

//桌面菜单
void WelcomeToMyDesk();

//用户注册
void Register();

//登录
int Login();

//找回密码
void Reback();

//创建文件函数
void Create_File();

//登录菜单
void WelcomMenu();