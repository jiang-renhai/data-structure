#define _CRT_SECURE_NO_WARNINGS 1
#include"SList.h"

enum {
	_eXIT_,      //0
	_inputtail_, //1
	_print_,     //2
	_storage_,	 //3
	_loading_,	 //4
	_find_,
	_erase_,
	_insertafter_,
	_modify_,
	_dressingbyscreening_,
	_statistics_,
	_bubblesort_,
	_mathmenu_,
	_judge_,
	_destory_,
	_excel_
};

int main() {
	SLTCBANode* plist = NULL;//定义一个结构体指针，用来传参
	int input;
	int i = 0;
	Create_File();//创建储存用户账号密码的文件
	//登录界面
	while (1) {
		WelcomMenu();
		setbuf(stdin, NULL);//清除缓存
		scanf("%d", &input);
		switch (input) {
		case 1: {
			//注册
			Register();
			system("pause");
			system("cls");
			break;
		}
		case 2: {
			//登录
			i = Login();
			system("pause");
			system("cls");
			break;
		}
		case 3: {
			//找回密码
			Reback();
			//system("pause");
			//system("cls");
			break;
		}
		case 0: {
			printf("退出系统\n");
			return 0;
			break;
		}
		default: {
			printf("选择错误，请重新选择\n");
			break;
			system("pause");
			system("cls");
		}
		}
		if (i == 1) {
			break;
		}
	}


	//操作界面
	while (1) {
		WelcomeToMyDesk();
		int input = 0;
		scanf("%d", &input);
		switch (input) {
		case _eXIT_: {
			printf("退出\n");
			return 0;
			break;
		}
		case _inputtail_: {
			//尾插
			InputTailCBAPlayer(&plist);
			break;
		}
		case _print_: {
			//打印
			SListPrint(plist);
			system("pause");
			system("cls");
			break;
		}
		case _storage_: {
			//文件存储，从IDE存到.txt文件
			StorageCBAPlayer(plist);
			break;
		}
		case _loading_: {
			//文件加载，从.txt文本文档读出
			LoadingCBAPlayer(&plist);
			break;
		}
		case _find_: {
			//查找
			SListFind(plist);
			break;
		}
		case _erase_: {
			//指定删除（根据身份证号删除人）
			EraseCBAPlayer(&plist);
			break;
		}
		case _insertafter_: {
			//指定插入后
			InsertCBAPlayerAfter(&plist);
			break;
		}
		case _modify_: {
			//修改成员信息
			ModifyCBAPlayer(plist);
			break;
		}
		case _dressingbyscreening_: {
			//筛选信息
			DressingByScreeningCBAPlayer(plist);
			break;
		}
		case _statistics_: {
			//统计球员数量
			int sum = StatisticsCBAPlayer(plist);
			printf("球员总共有：%d个人\n", sum);
			system("pause");//暂停
			system("cls");//清空
			break;
		}
		case _bubblesort_: {
			//排序
			BubbleSortCBAPlayer(&plist);
			system("pause");//暂停
			system("cls");//清空
			break;
		}
		case _mathmenu_: {
			//计算体脂率，学习等级，体育等级
			MathMenu();
			break;
		}
		case _judge_: {
			//判断是否进入CBA
			JudgeCBAPlayer(plist);
			//system("pause");//暂停
			//system("cls");//清空
			break;
		}
		case _destory_: {
			//清空
			SListDestory(&plist);
			break;
		}
		case _excel_: {
			//报表
			Excel(plist);
			break;
		}
		default: {
			printf("输入错误，请重新输入\n");
			system("pause");//暂停
			system("cls");//清空
			break;
		}

		}
	}
	return 0;
}