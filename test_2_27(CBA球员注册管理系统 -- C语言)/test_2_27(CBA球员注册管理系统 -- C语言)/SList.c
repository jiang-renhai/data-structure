﻿#define _CRT_SECURE_NO_WARNINGS 1
#include"SList.h"

//1.身份证
//2.姓名
//3.性别
//4.年龄
//5.球衣号码
//6.身高
//7.体重
//8.臂展
//9.体脂率
//10.运动等级
//11.学习等级

enum {//查找case
	Exit,
	_id,
	_name,
	_sex,
	_age,
	_number,
	_high,
	_weight,
	_reach,
	_bodyfatpercentage,
	_peresult,
	_learingscore
};

enum {//修改单人球员信息case
	_EXIT,
	_ID,
	_NAME,
	_SEX,
	_AGE,
	_NUMBER,
	_HIGH,
	_WEIGHT,
	_REACH,
	_BFP,
	_PER,
	_LS
};

enum {//筛选case
	_exit_,
	_age_,
	_high_,
	_weigh_,
	_reach_,
	_bfp_
};

//查找菜单界面
void FindMenu() {
	//0.退出程序
	//1.按照身份证号查
	//2.按照姓名查找
	//3.按照性别查找
	//4.年龄
	//5.球衣号码
	//6.身高
	//7.体重
	//8.臂展
	//9.体脂率
	//10.运动等级
	//11.学习等级
	printf("\t   ********************************************************\t\t\n");
	printf("\t   **************      欢迎进入查找菜单     ***************\t\t\n");
	printf("\t   ************     请按照选项进行选择功能     ************\t\t\n");
	printf("\t   ************ 0.退出          1.按身份证号查 ************\t\t\n");
	printf("\t   ************ 2.按姓名查      3.按性别查     ************\t\t\n");
	printf("\t   ************ 4.按年龄查      5.按球衣号码查 ************\t\t\n");
	printf("\t   ************ 6.按身高查      7.按体重查     ************\t\t\n");
	printf("\t   ************ 8.按臂展查      9.按体脂率查   ************\t\t\n");
	printf("\t   ************ 10.按运动等级查 11.按学习等级查************\t\t\n");
	printf("\t   ********************************************************\t\t\n");
	printf("\n");
	printf("\t   **************           请选择           **************\t\t\n");
	//printf("1,2,3,4,5,6,7,8,9,10,11,0");
}

//修改菜单界面
void Changemenu() {
	//0.退出
	//1.改身份证号
	//2.改姓名
	//3.改性别
	//4.改年龄
	//5.改球衣号码
	//6.改身高
	//7.改体重
	//8.改臂展
	//9.改体脂率
	//10.改运动等级
	//11.改学习等级
	printf("\t   ********************************************************\t\t\n");
	printf("\t   **************      欢迎进入修改菜单     ***************\t\t\n");
	printf("\t   ************     请按照选项进行选择功能     ************\t\t\n");
	printf("\t   ************ 0.退出          1.改身份证号   ************\t\t\n");
	printf("\t   ************ 2.改姓名        3.改性别       ************\t\t\n");
	printf("\t   ************ 4.改年龄        5.改球衣号码   ************\t\t\n");
	printf("\t   ************ 6.改身高        7.改体重       ************\t\t\n");
	printf("\t   ************ 8.改臂展        9.改体脂率     ************\t\t\n");
	printf("\t   ************ 10.改运动等级   11.改学习等级  ************\t\t\n");
	printf("\t   ********************************************************\t\t\n");
	printf("\n");
	printf("\t   **************           请选择           **************\t\t\n");
	//printf("12132423\n");
}

//筛选菜单界面
void SelectMenu() {
	//  _exit_, 0
	//	_age_,
	//	_high_,
	//	_weigh_,
	//	_reach_,
	//	_bfp_,
	//	_per_,
	//	_ls_
	printf("\t   ********************************************************\t\t\n");
	printf("\t   **************      欢迎进入筛选菜单     ***************\t\t\n");
	printf("\t   ************     请按照选项进行选择功能     ************\t\t\n");
	printf("\t   ************ 0.退出          1.筛选年龄     ************\t\t\n");
	printf("\t   ************ 2.筛选身高      3.筛选体重     ************\t\t\n");
	printf("\t   ************ 4.筛选臂展      5.筛选体脂率   ************\t\t\n");
	printf("\t   ************ 6.筛选体育等级  7.筛选学习等级 ************\t\t\n");
	printf("\t   ********************************************************\t\t\n");
	printf("\n");
	printf("\t   **************           请选择           **************\t\t\n");
	//printf("7654321\n");
}

//通过身份证查找
void FindByID(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	char IDCard[20] = { 0 };//用于判断身份证号是否相同
	printf("请输入身份证号码>\n");
	scanf("%s", IDCard);
	//int len = strlen(cur->cbaplayer.ID);
	while (cur != NULL) {
		if (0 == (strcmp(cur->cbaplayer.ID ,IDCard))) { //库函数进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过姓名查找
void FindByName(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	char Name[20] = { 0 };
	printf("请输入球员姓名>\n");
	scanf("%s", Name);
	while (cur != NULL) {
		if (0 == (strcmp(cur->cbaplayer.name, Name))) { //库函数进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过性别查找
void FindBySex(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	char Sex[3] = { 0 };
	printf("请输入球员性别>\n");
	scanf("%s", Sex);
	while (cur != NULL) {
		if (0 == (strcmp(cur->cbaplayer.sex, Sex))) { //库函数进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过年龄查找（数字直接比较）
void FindByAge(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	int Age = 0;
	printf("请输入球员年龄>\n");
	scanf("%d", &Age);
	while (cur != NULL) {
		if (Age == cur->cbaplayer.age) { //数字直接进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过球衣号码进行查找
void FindByNumber(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	int Number = 0;
	printf("请输入球员球衣号码>\n");
	scanf("%d", &Number);
	while (cur != NULL) {
		if (Number == cur->cbaplayer.number) { //数字直接进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过身高查找
void FindByHigh(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	float High = 0.0f;
	printf("请输入球员身高>\n");
	scanf("%f", &High);
	while (cur != NULL) {
		if (High == cur->cbaplayer.high) { //数字直接进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过体重查找
void FindByWeigh(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	float Weigh = 0.0f;
	printf("请输入球员体重>\n");
	scanf("%f", &Weigh);
	while (cur != NULL) {
		if (Weigh == cur->cbaplayer.weight) { //数字直接进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过臂展查找
void FindByReach(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	float Reach = 0.0f;
	printf("请输入球员臂展>\n");
	scanf("%f", &Reach);
	while (cur != NULL) {
		if (Reach == cur->cbaplayer.reach) { //数字直接进行比较
			flag = 1;
			printf("\n球员的信息为\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过体脂率查找
void FindByBodyFatPercentage(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	float BodyFatPercentage = 0.0f;
	printf("请输入球员体脂率>\n");
	scanf("%f", &BodyFatPercentage);
	while (cur != NULL) {
		if (BodyFatPercentage == cur->cbaplayer.BodyFatPercentage) { //数字直接进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过体育等级查找
void FindByPEReasult(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	char PEReasult[15] = { 0 };
	printf("请输入体育等级>\n");
	scanf("%s", PEReasult);
	while (cur != NULL) {
		if (0 == (strcmp(cur->cbaplayer.PEResult, PEReasult))) { //库函数进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//通过学习等级进行查找
void FindByLearingScore(const SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	char LearingScore[15] = { 0 };
	printf("请输入学习等级>\n");
	scanf("%s", LearingScore);
	while (cur != NULL) {
		if (0 == (strcmp(cur->cbaplayer.LearingScore, LearingScore))) { //库函数进行比较
			flag = 1;
			printf("\n球员的信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//增加一个结点
SLTCBANode* BuyListNode() {
	SLTCBANode* newnode = (SLTCBANode*)malloc(sizeof(SLTCBANode));
	if (newnode == NULL) {
		printf("malloc fail\n");
		exit(-1);
	}
	newnode->next = NULL;
}

//增加球员信息(尾插法)
void InputTailCBAPlayer(SLTCBANode** pphead) {
	assert(pphead);
	SLTCBANode* newnode = BuyListNode();//增加一个结点

	if (*pphead == NULL) {
		*pphead = newnode;
	}
	else {
		//找到尾结点
		SLTCBANode* tail = *pphead;
		//链表往后遍历
		while (tail->next != NULL) {
			tail = tail->next;
		}
		tail->next = newnode;
	}

	printf("请输入球员身份证号码>\n");
	scanf("%s", newnode->cbaplayer.ID);
	printf("请输入球员姓名(英文)>\n");
	scanf("%s", newnode->cbaplayer.name);
	printf("请输入球员性别(女f,男m)>\n");
	scanf("%s", newnode->cbaplayer.sex);
	printf("请输入球员年龄>\n");
	scanf("%d", &newnode->cbaplayer.age);
	printf("请输入球员的球衣号码>\n");
	scanf("%d", &newnode->cbaplayer.number);
	printf("请输入球员身高(cm)>\n");
	scanf("%f", &newnode->cbaplayer.high);
	printf("请输入球员体重(kg)>\n");
	scanf("%f", &newnode->cbaplayer.weight);
	printf("请输入球员臂展(cm)>\n");
	scanf("%f", &newnode->cbaplayer.reach);
	printf("请输入球员体脂率(小数)>\n");
	scanf("%f", &newnode->cbaplayer.BodyFatPercentage);
	printf("请输入球员运动等级(优秀outstanding,良好good,及格pass)>\n");
	scanf("%s", newnode->cbaplayer.PEResult);
	printf("请输入球员学习等级(优秀outstanding,良好good,及格pass)>\n");
	scanf("%s", newnode->cbaplayer.LearingScore);

	system("pause");//暂停
	system("cls");//清屏
}

//打印(空链表也可以打印)
void SListPrint(const SLTCBANode* phead) {
	//assert(phead);
	SLTCBANode* cur = phead;
	int i = 1;
	if (cur == NULL) {
		printf("数据为空，请输入后再打印\n");
	}
	while (cur != NULL) {
		printf("\n");
		printf("\t\t\t***这是第%d个球员的个人信息***\t\t\t\n", i);
		printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		i++;
		cur = cur->next;
	}
}

//查找（按照不同方式查找）
void SListFind(const SLTCBANode* phead) {
	//assert(phead);
	system("cls");
	SLTCBANode* cur = phead;
	if (cur == NULL) {
		printf("链表为空，查不了人\n");
		system("pause");//暂停
		system("cls");//清屏
		return;
	}
	int input = 0;
	do {
		FindMenu();//查找菜单
		scanf("%d", &input);
		switch (input) {

		case Exit: {//退出，不进行查找
			printf("退出查找\n");
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _id: {
			//按照身份证查找
			FindByID(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _name: {
			//按照姓名查找（与上个接口函数代码相似）
			FindByName(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _sex: {
			//按照性别查找
			FindBySex(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _age: {
			//按照年龄查找
			FindByAge(cur);//有所不同，数字直接比较
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _number: {
			//按照球衣号码查找
			FindByNumber(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _high: {
			//按照身高查找
			FindByHigh(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _weight: {
			//按照体重查找
			FindByWeigh(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _reach: {
			//按照臂展查找
			FindByReach(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _bodyfatpercentage: {
			//按照体脂率查找
			FindByBodyFatPercentage(cur); 
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _peresult: {
			//按照体育等级查找
			FindByPEReasult(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _learingscore: {
			//按照学习等级查找
			FindByLearingScore(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		default:
			printf("输入有误，请重新输入!\n");
			system("pause");//暂停
			system("cls");//清屏
			break;

		}
	} while (input);
}

//修改单个球员信息
void ModifyCBAPlayer(SLTCBANode* phead) {
	system("cls");
	//assert(phead);
	SLTCBANode* cur = phead;
	if (cur == NULL) {
		printf("链表无人，请重新输入\n");
		system("pause");//暂停
		system("cls");//清屏
		return;
	}
	//球员的身份证号
	char IDCard[30] = { 0 };
	printf("请输入球员的身份证号>\n");
	scanf("%s", IDCard);
	//遍历链表
	while (cur != NULL) {
		//用身份证判断球员是否找到（身份证唯一）
		if (0 == strcmp(cur->cbaplayer.ID, IDCard)) {//找到了
			printf("您找到的是%s球员\n", cur->cbaplayer.name);
			//ChangeMenu();//修改菜单界面
			int input = 0;
			do {
				Changemenu();//修改菜单界面
				printf("请输入需要修改的信息>");
				scanf("%d", &input);
				switch (input) {
				case _EXIT: {
					printf("退出修改\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _ID: {
					printf("请输入需要修改的身份证号>");
					scanf("%s", cur->cbaplayer.ID);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _NAME: {
					printf("请输入需要修改的球员姓名>");
					scanf("%s", cur->cbaplayer.name);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _SEX: {
					printf("请输入需要修改的球员性别>");
					scanf("%s", cur->cbaplayer.sex);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _AGE: {
					printf("请输入需要修改的球员年龄>");
					scanf("%d", &cur->cbaplayer.age);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _NUMBER: {
					printf("请输入需要修改的球员球衣号码>");
					scanf("%d", &cur->cbaplayer.number);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _HIGH: {
					printf("请输入需要修改的球员身高>");
					scanf("%f", &cur->cbaplayer.high);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _WEIGHT: {
					printf("请输入需要修改的球员体重>");
					scanf("%f", &cur->cbaplayer.weight);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _REACH: {
					printf("请输入需要修改的球员臂展>");
					scanf("%f", &cur->cbaplayer.reach);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _BFP: {
					printf("请输入需要修改的球员体脂率>");
					scanf("%f", &cur->cbaplayer.BodyFatPercentage);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _PER:{
					printf("请输入需要修改的球员体育等级>");
					scanf("%s", cur->cbaplayer.PEResult);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				case _LS: {
					printf("请输入需要修改的球员学习等级>");
					scanf("%s", cur->cbaplayer.LearingScore);
					printf("修改成功\n");
					system("pause");//暂停
					system("cls");//清屏
					break;
				}
				default: {
					printf("输入有误，请重新输入!\n");
					break;
				}
				}
			} while (input);

			break;
		}
		cur = cur->next;
	}

	if (cur == NULL) {
		printf("找不到球员信息，请重新输入!\n");
		system("pause");//暂停
		system("cls");//清屏
	}
}

//指插（找到位置再插入到位置的后面）
void InsertCBAPlayerAfter(SLTCBANode** pphead) {
	//assert(pphead);
	int pos = 0;
	int ret = 0;
	SLTCBANode* prev = *pphead;//prev为头结点
	if (prev == NULL) {//空链表
		printf("还未存入球员信息，请重试！\n");
		system("pause");//暂停
		system("cls");//清屏
		return;
	}
	printf("请输入要插入哪个人的后面（序号）\n");
	scanf("%d", &pos);//找位置
	while (prev) {//算算链表数
		ret++;
		prev = prev->next;
	}
	if (pos > ret || pos <= 0) {//用户输入位置过大
		printf("找不到需要插入的位置，请重新试！\n");
		printf("此时只存了%d个球员的信息\n", ret);
		system("pause");//暂停
		system("cls");//清屏
		return;
	}
	SLTCBANode* temp = NULL;//临时空间
	if (pos == 1) {//在头结点
		SLTCBANode* newnode = BuyListNode();//建立一个新的结点
		printf("请输入球员身份证号码>\n");
		scanf("%s", newnode->cbaplayer.ID);
		printf("请输入球员姓名(英文)>\n");
		scanf("%s", newnode->cbaplayer.name);
		printf("请输入球员性别(女f,男m)>\n");
		scanf("%s", newnode->cbaplayer.sex);
		printf("请输入球员年龄>\n");
		scanf("%d", &newnode->cbaplayer.age);
		printf("请输入球员的球衣号码>\n");
		scanf("%d", &newnode->cbaplayer.number);
		printf("请输入球员身高(cm)>\n");
		scanf("%f", &newnode->cbaplayer.high);
		printf("请输入球员体重(kg)>\n");
		scanf("%f", &newnode->cbaplayer.weight);
		printf("请输入球员臂展(cm)>\n");
		scanf("%f", &newnode->cbaplayer.reach);
		printf("请输入球员体脂率(小数)>\n");
		scanf("%f", &newnode->cbaplayer.BodyFatPercentage);
		printf("请输入球员运动等级(优秀outstanding,良好good,及格pass)>\n");
		scanf("%s", newnode->cbaplayer.PEResult);
		printf("请输入球员学习等级(优秀outstanding,良好good,及格pass)>\n");
		scanf("%s", newnode->cbaplayer.LearingScore);

		newnode->next = prev->next;
		prev->next = newnode;

		printf("增加成功\n");
		system("pause");//暂停
		system("cls");//清屏
		return;
	}
	else if (pos == ret) {
		InputTailCBAPlayer(&prev);//尾插
	}
	else {//不在头结点
		while (prev->next != NULL) {
			//找相等的位置，先定义pos1，再++，直至与pos相同，找到位置
			int pos1 = 1;
			pos1++;
			prev = prev->next;
			if (pos == pos1) {//相等进行插入操作
				SLTCBANode* newnode1 = BuyListNode();//建立一个新的结点
				printf("请输入球员身份证号码>\n");
				scanf("%s", newnode1->cbaplayer.ID);
				printf("请输入球员姓名(英文)>\n");
				scanf("%s", newnode1->cbaplayer.name);
				printf("请输入球员性别(女f,男m)>\n");
				scanf("%s", newnode1->cbaplayer.sex);
				printf("请输入球员年龄>\n");
				scanf("%d", &newnode1->cbaplayer.age);
				printf("请输入球员的球衣号码>\n");
				scanf("%d", &newnode1->cbaplayer.number);
				printf("请输入球员身高(cm)>\n");
				scanf("%f", &newnode1->cbaplayer.high);
				printf("请输入球员体重(kg)>\n");
				scanf("%f", &newnode1->cbaplayer.weight);
				printf("请输入球员臂展(cm)>\n");
				scanf("%f", &newnode1->cbaplayer.reach);
				printf("请输入球员体脂率(小数)>\n");
				scanf("%f", &newnode1->cbaplayer.BodyFatPercentage);
				printf("请输入球员运动等级(优秀outstanding,良好good,及格pass)>\n");
				scanf("%s", newnode1->cbaplayer.PEResult);
				printf("请输入球员学习等级(优秀outstanding,良好good,及格pass)>\n");
				scanf("%s", newnode1->cbaplayer.LearingScore);

				newnode1->next = prev->next;
				prev->next = newnode1;

				printf("增加成功\n");
				system("pause");//暂停
				system("cls");//清屏
				return;
			}
			//prev = prev->next;//比控制范围多了的数据，指向空，越界（可以在此进行调试，将这段代码屏蔽掉进行调试）
			if (prev == NULL) {//越界
				printf("越界，插入不了！\n");
				break;
			}
		}
	}
	//if (prev == NULL) {
	//	printf("超出范围\n");
	//	system("pause");//暂停
	//	system("cls");//清屏
	//}
}

//指删（找到位置再删除）
void EraseCBAPlayer(SLTCBANode** pphead) {
	//assert(pphead);
	SLTCBANode* cur = *pphead;
	if (cur == NULL) {//空链表
		printf("还未存入球员信息，请重试！\n");
		system("pause");//暂停
		system("cls");//清屏
		return;
	}
	//用身份证进行找人
	char IDCard[30] = { 0 };
	printf("请输入需要删除球员的身份证号>\n");
	scanf("%s", IDCard);
	if (0 == strcmp(cur->cbaplayer.ID, IDCard)) {//在头结点上
		SLTCBANode* Next = (*pphead)->next;//解引用找一级指针，
		free(*pphead);//释放头结点
		*pphead = Next;//头结点被Next代替

		printf("删除成功\n");
		system("pause");//暂停
		system("cls");//清屏
		return;
	}
	else {//不在头结点上      //特色：快慢指针，可以先把Next屏蔽掉，进行调试，发现会越界，导致删除的很奇葩
		SLTCBANode* prev = *pphead;//用来判断是否到位置
		SLTCBANode* Delet = *pphead;//用来删除的，记忆Next->next
		SLTCBANode* Next = *pphead;//用来记忆到位置前一个链的位置的
		
		while (prev->next != NULL) {
			prev = prev->next;//先行增加找到位置
			if (0 == strcmp(prev->cbaplayer.ID, IDCard)) {
				Delet = Next->next;
				//prev->next = Delet->next;
				//prev->next = prev->next->next;//直接跳过中间一个链到下一个链进行链接
				Next->next = Next->next->next;
				free(Delet);
				//free(prev->next);
				printf("删除成功\n");
				system("pause");//暂停
				system("cls");//清屏
				return;
			}
			Next = Next->next;//后增加，当进入if时候比prev少一次
			if (prev->next == NULL) {//越界
				printf("查无此人，删除不了！\n");
				system("pause");//暂停
				system("cls");//清屏
				break;
			}
		}
	}
	if (cur == NULL) {
		printf("超出范围\n");
		system("pause");//暂停
		system("cls");//清屏
	}
}

//按照年龄筛选
void SelectByAge(SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	int AgeMin = 0;
	int AgeMax = 0;
	printf("请输入需要筛查球员的年龄范围>\n");
	scanf("%d %d", &AgeMin, &AgeMax);
	while (cur != NULL) {
		if (cur->cbaplayer.age <= AgeMax && cur->cbaplayer.age >= AgeMin) { 
			flag = 1;
			printf("\n球员信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//根据身高筛查
void SelectByHigh(SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	float HighMin = 0.0f;
	float HighMax = 0.0f;
	printf("请输入需要筛查球员的身高范围>\n");
	scanf("%f %f", &HighMin, &HighMax);
	while (cur != NULL) {
		if (cur->cbaplayer.high <= HighMax && cur->cbaplayer.high >= HighMin) {
			flag = 1;
			printf("\n球员信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//按照体重进行筛选
void SelectByWeigh(SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	float WeighMin = 0.0f;
	float WeighMax = 0.0f;
	printf("请输入需要筛查球员的体重范围>\n");
	scanf("%f %f", &WeighMin, &WeighMax);
	while (cur != NULL) {
		if (cur->cbaplayer.weight <= WeighMax && cur->cbaplayer.weight >= WeighMin) {
			flag = 1;
			printf("\n球员信息为\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//根据臂展筛选
void SelectByReach(SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	float ReachMin = 0.0f;
	float ReachMax = 0.0f;
	printf("请输入需要筛查球员的臂展范围>\n");
	scanf("%f %f", &ReachMin, &ReachMax);
	while (cur != NULL) {
		if (cur->cbaplayer.reach <= ReachMax && cur->cbaplayer.reach >= ReachMin) {
			flag = 1;
			printf("\n球员信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//按照体脂率进行筛查
void SlectByBFP(SLTCBANode* cur) {
	int flag = 0;//判断是否相同的标志
	float BFPMin = 0.0f;
	float BFPMax = 0.0f;
	printf("请输入需要筛查球员的体脂率范围>\n");
	scanf("%f %f", &BFPMin, &BFPMax);
	while (cur != NULL) {
		if (cur->cbaplayer.BodyFatPercentage <= BFPMax && cur->cbaplayer.BodyFatPercentage >= BFPMin) {
			flag = 1;
			printf("\n球员信息为:\n");
			printf("球员身份证为:%s\n球员姓名为:%s\n球员性别为:%s\n球员年龄为:%d\n球员球衣号码为:%d\n球员身高为:%.1f\n球员体重为:%.1f\n球员臂展为:%.1f\n球员体脂率为:%.1f\n球员体育等级为:%s\n球员学习等级为:%s\n",
				cur->cbaplayer.ID,
				cur->cbaplayer.name,
				cur->cbaplayer.sex,
				cur->cbaplayer.age,
				cur->cbaplayer.number,
				cur->cbaplayer.high,
				cur->cbaplayer.weight,
				cur->cbaplayer.reach,
				cur->cbaplayer.BodyFatPercentage,
				cur->cbaplayer.PEResult,
				cur->cbaplayer.LearingScore);
		}
		cur = cur->next;
	}
	if (flag == 0) {
		printf("查无此人\n");
	}
}

//筛选（根据不同条件筛选）
void DressingByScreeningCBAPlayer(const SLTCBANode* phead) {
	//assert(phead);
	system("cls");
	SLTCBANode* cur = phead;
	if (cur == NULL) {
		printf("链表还未存入信息，请重新输入\n");
		system("pause");
		system("cls");
		return;
	}
	int input = 0;
	do {
		SelectMenu();
		scanf("%d", &input);
		switch (input) {
		case _exit_: {
			printf("退出查找\n");
			system("pause");//暂停
			system("cls");//清屏
			break;		
		}
		case _age_: {
			SelectByAge(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _high_: {
			SelectByHigh(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _weigh_: {
			SelectByWeigh(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _reach_: {
			SelectByReach(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		case _bfp_: {
			SlectByBFP(cur);
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		default: {
			printf("输入错误，请重新输入\n");
			system("pause");//暂停
			system("cls");//清屏
			break;
		}
		}
	} while (input);
}

//统计球员人数
int StatisticsCBAPlayer(const SLTCBANode* phead) {
	//assert(phead);
	SLTCBANode* cur = phead;
	if (phead == 0) {//空链表为0
		return 0;
	}
	int count = 1;//总有一个头结点
	while (cur->next != NULL) {
		count++;
		cur = cur->next;
	}
	return count;
}

//销毁链表
void SListDestory(SLTCBANode** pphead) {
	SLTCBANode* cur = *pphead;
	while (cur) {//从前往后进行free，再把头结点指向NULL
		SLTCBANode* next = cur->next;
		free(cur);
		cur = next;
	}
	*pphead = NULL;
	printf("销毁成功\n");
	system("pause");
	system("cls");
}

//排序（按照身份证号进行排序）
void BubbleSortCBAPlayer(SLTCBANode** pphead) {
	//assert(pphead);
	SLTCBANode* cur = *pphead;
	if (cur == NULL) {
		printf("链表为空，无法进行排序\n");
		return;
	}
	printf("按照身份证号进行排序\n");
	printf("退出排序请按0\n");
	printf("升序排序请按1\n");
	printf("降序排序请按2\n");
	printf("   请输入\n");
	int input = 0;
	//通过两个指针进行前后对比的操作，一个指针比另一个指针先走一步
	//后续步长都是一样的，就是进行前后对比操作
	SLTCBANode* prev1 = NULL;//慢走一步指针
	SLTCBANode* prev2 = NULL;//快走一步指针
	SLTCBANode temp = { 0 };//临时交换空间
	scanf("%d", &input); //通过调试，原本这里加了do-while语句，发现加上以后是陷入死循环，可进行调试进行解决
	switch (input) {
	case 0:
		printf("程序退出\n");
		break;
	case 1: {//升序
		//循环遍历
		for (prev1 = cur; prev1 != NULL; prev1 = prev1->next) {
			prev2 = prev1->next;
			//循环
			while (prev2 != NULL) {
				if (strcmp(prev1->cbaplayer.ID, prev2->cbaplayer.ID) > 0) {//库函数
					temp = *prev2;//整体数据进行交换
					*prev2 = *prev1;
					*prev1 = temp;

					temp.next = prev2->next;//地址交换
					prev2->next = prev1->next;
					prev1->next = temp.next;
				}
				prev2 = prev2->next;//快指针往后移动
			}
		}
		printf("排序成功\n");
		//SListPrint(cur);
		break;
	}
	case 2:{//降序排序
		//循环遍历
		for (prev1 = cur; prev1 != NULL; prev1 = prev1->next) {
			prev2 = prev1->next;
			//循环
			while (prev2 != NULL) {
				if (strcmp(prev1->cbaplayer.ID, prev2->cbaplayer.ID) < 0) {//库函数
					temp = *prev2;//整体数据进行交换
					*prev2 = *prev1;
					*prev1 = temp;

					temp.next = prev2->next;//地址交换
					prev2->next = prev1->next;
					prev1->next = temp.next;
				}
				prev2 = prev2->next;//快指针往后移动
			}
		}
		printf("排序成功\n");
		//SListPrint(cur);
		break;
	}
	default: {
		printf("输入错误，请重新输入\n");
		system("pause");//暂停
		system("cls");//清屏
		break;
	}
	}
	//SListPrint(cur);//调用函数直接进行打印
}

void MenuJugement() {
	printf("\n判断界面\n");
	printf("查大家请按1\n");
	printf("查自己请按2\n");
	printf("退出请按0\n");
	printf("\n  请输入\n");
}

//判断男生进入CBA
void JudgeCBAPlayermale(SLTCBANode** prev) {
	SLTCBANode* cur = *prev;
	if (strlen(cur->cbaplayer.ID) == 18 && cur->cbaplayer.BodyFatPercentage <= 10 &&
		strcmp(cur->cbaplayer.PEResult, "outstanding") == 0 &&
		strcmp(cur->cbaplayer.LearingScore, "outstanding") == 0) {
		printf("恭喜您已经通过初试，请回家等候复试!\n");
	}

	else {
		printf("非常抱歉，您未通过CBA的考核标准!\n");
		printf("以下是您不符合的问题，如下:\n");
		if (strlen(cur->cbaplayer.ID) != 18) {
			printf("身份证输入有误，进入不了CBA，请退出重试!\n");
		}
		if (cur->cbaplayer.BodyFatPercentage > 10) {
			printf("控制一下体脂率!\n");
		}
		if (strcmp(cur->cbaplayer.PEResult, "outstanding") != 0) {
			printf("提升提升体育成绩再来!\n");
		}
		if (strcmp(cur->cbaplayer.LearingScore, "outstanding") != 0) {
			printf("提升提升学习成绩再来!\n");
		}
	}
	
}

//判断女生进入CBA
void JudgeCBAPlayerfemale(SLTCBANode** prev) {
	SLTCBANode* cur = *prev;
	if (strlen(cur->cbaplayer.ID) == 18 && cur->cbaplayer.BodyFatPercentage <= 12 &&
		strcmp(cur->cbaplayer.PEResult, "outstanding") == 0 &&
		strcmp(cur->cbaplayer.LearingScore, "outstanding") == 0) {
		printf("恭喜您已经通过初试，请回家等候复试!\n");
	}
	else {
		printf("非常抱歉，您未通过CBA的考核标准!\n");
		printf("以下是您不符合的问题，如下:\n");
		if (strlen(cur->cbaplayer.ID) != 18) {
			printf("身份证输入有误，进入不了CBA，请退出重试!\n");
		}
		if (cur->cbaplayer.BodyFatPercentage > 12) {
			printf("控制一下体脂率!\n");
		}
		if (strcmp(cur->cbaplayer.PEResult, "outstanding") != 0) {
			printf("提升提升体育成绩再来!\n");
		}
		if (strcmp(cur->cbaplayer.LearingScore, "outstanding") != 0) {
			printf("提升提升学习成绩再来!\n");
		}
	}
}

//判断所有人是否进入CBA
void JudgeCBAPlayer1(SLTCBANode* phead) {
	//assert(phead);
	SLTCBANode* cur = phead;
	if (cur == NULL) {//空链表
		printf("无人，请重新输入!\n");
		system("pause");//暂停
		system("cls");//清空	
		return;
	}
	//遍历链表
	//eg：体脂率10以下,体育和学习成绩都优秀
	while (cur != NULL) {
		//都用单语句进行判断，到最后输出的是需要注意的问题
		//先男再女进行判断
		printf("%s同学\n", cur->cbaplayer.name);
		if (strcmp(cur->cbaplayer.sex, "m") == 0) {
			JudgeCBAPlayermale(&cur);//一级指针的地址

		}
		else if (strcmp(cur->cbaplayer.sex, "f") == 0) {
			JudgeCBAPlayerfemale(&cur);//一级指针的地址
		}
		else{
			printf("输入有误\n");
		}

		cur = cur->next;
	}
}

//判断是否进入CBA
void JudgeCBAPlayer(SLTCBANode* phead) {
	//assert(phead);
	system("cls");
	SLTCBANode* cur = phead;
	if (cur == NULL) {//空链表
		printf("无人，请重新输入!\n");
		system("pause");//暂停
		system("cls");//清空	
		return;
	}
	int input = 0;
	do {
		MenuJugement();
		scanf("%d", &input);
		switch (input) {
		case 0: {
			printf("退出程序\n");
			system("pause");
			system("cls");
			break;
		}
		case 1: {//判断所有人
			JudgeCBAPlayer1(cur);
			system("pause");
			system("cls");
			break;
		}
		case 2: {//判断个人
			char sex_[5];
			char name_[20];
			int flag = 0;
			printf("请输入姓名\n");
			scanf("%s", name_);
			printf("请输入性别\n");
			scanf("%s", sex_);
			while (cur) {
				if (strcmp(sex_, "f") == 0 && strcmp(name_, cur->cbaplayer.name)==0) {
					JudgeCBAPlayerfemale(&cur);//一级指针的地址
					flag = 1;
					break;
				}
				else if (strcmp(sex_, "m") == 0 && strcmp(name_, cur->cbaplayer.name)==0) {
					JudgeCBAPlayermale(&cur);
					flag = 1;
					break;
				}
				/*else {
					printf("输入错误\n");
				}*/
				cur = cur->next;
			}
			if (flag == 0) {
				printf("查无此人\n");
			}
			system("pause");
			system("cls");
			break;
		}

		default: {
			printf("输入有误,请重新输入\n");
			system("pause");
			system("cls");
			break;
		}
		}
	} while (input);

	////遍历链表
	////eg：体脂率10以下,体育和学习成绩都优秀
	//while (cur != NULL) {
	//	//都用单语句进行判断，到最后输出的是需要注意的问题
	//	//先男再女进行判断
	//	printf("%s同学\n", cur->cbaplayer.name);
	//	if (strcmp(cur->cbaplayer.sex, "m") == 0) {
	//		JudgeCBAPlayermale(&cur);//一级指针的地址

	//	}
	//	else if (strcmp(cur->cbaplayer.sex, "f") == 0) {
	//		JudgeCBAPlayerfemale(&cur);//一级指针的地址
	//	}
	//	else {
	//		printf("输入有误\n");
	//	}

	//	cur = cur->next;
	//}
}

//计算菜单
void MathMenuInterface() {
	printf("\t   ******************************************************\t\t\n");
	printf("\t   ****************    欢迎来到计算菜单     **************\t\t\n");
	printf("\t   ***************   0.退出     1.体脂率    *************\t\t\n");
	printf("\t   ***************   2.学习等级 3.体育等级  *************\t\t\n");
	printf("\t   ***************          请选择          *************\t\t\n");
	printf("\t   ******************************************************\t\t\n");
}

//计算（体脂率，学习成绩，体育成绩）
void MathMenu() {
	system("cls");
	int input = 0;
	float math = 0.0f;
	float chinese = 0.0f;
	float english = 0.0f;
	float fiftymeters = 0.0f;      //10
	float basketballskills = 0.0f; //60
	float Standinglongjump = 0.0f; //10
	float eightymeters = 0.0f;     //20
	MathMenuInterface();
	do {
		printf("请选择\n");
		scanf("%d", &input);
		switch (input) {
		case 0: {
			printf("退出程序\n");
			break;
		}
		case 1:{
			printf("请输入性别(男male，女female):>\n");
			char Sex[10];
			scanf("%s", Sex);
			int i = strlen(Sex);
			if (i == 4) {
				printf("请分别输入腰围(cm)和体重(kg)\n");
				float waistline = 0.0f;
				float weight = 0.0f;
				scanf("%f %f", &waistline, &weight);
				float BFP = ((waistline * 0.74) - (weight * 0.082 + 44.74)) / weight * 100;
				printf("体脂率为%.1f\n", BFP);
			}
			else {
				printf("请分别输入腰围(cm)和体重(kg)\n");
				float waistline1 = 0.0f;
				float weight1 = 0.0f;
				scanf("%f %f", &waistline1, &weight1);
				float BFP = ((waistline1 * 0.74) - (weight1 * 0.082 + 34.89)) / weight1 * 100;
				printf("体脂率为%.1f\n", BFP);
			}
			break;
		}
		case 2:
			//学习等级
			printf("请分别输入语文数学和英语分数(百分制)\n");
			scanf("%f %f %f", &chinese, &math, &english);
			float avarage = (chinese + math + english) / 3;
			if (avarage >= 85 && avarage <= 100) {
				printf("outstanding\n");
			}
			else if (avarage >= 75 && avarage < 85) {
				printf("good\n");
			}
			else if (avarage >= 60 && avarage < 75) {
				printf("pass\n");
			}
			else {
				printf("no pass\n");
			}
			break;
		case 3: {
			//体育等级
			printf("请输入50米，篮球技巧，立定跳远和800米跑步成绩（百分制）\n");
			scanf("%f %f %f %f", &fiftymeters, &basketballskills, &Standinglongjump, &eightymeters);
			float sum = 0.1 * fiftymeters + 0.6 * basketballskills + 0.1 * Standinglongjump + 0.2 * eightymeters;
			if (sum >= 90) {
				printf("outstanding\n");
			}
			else if (sum >= 80 && sum < 90) {
				printf("good\n");
			}
			else if (sum >= 70 && sum < 80) {
				printf("pass\n");
			}
			else {
				printf("no pass\n");
			}
			break;
		}
		}

	} while (input);
	system("pause");//暂停
	system("cls");//清空
}

//文件的存储（将文件存在.txt文件中）
void StorageCBAPlayer(SLTCBANode* phead) {
	//assert(phead);
	if (phead == NULL) {
		printf("无文件存储\n");
		system("pause");
		system("cls");
		return;
	}
	//打开文件
	FILE* pFile = fopen("D:\\GITTE chuantimu2（数据结构+C++）\\test_2_27(CBA球员注册管理系统 -- C语言)\\CBAPlayer Registration management system.txt", "w");
	FILE* pFile1 = fopen("D:\\GITTE chuantimu2（数据结构+C++）\\test_2_27(CBA球员注册管理系统 -- C语言)\\CBAPlayer Registration management system2.txt", "w");
	//空
	if (pFile == NULL) {
		perror("fopen->pFile");
		return;
	}
	if (pFile1 == NULL) {
		perror("fopen->pFile1");
		return;
	}
	//文件操作
	else {
		SLTCBANode* cur = phead;
		while (cur != NULL) {
			fprintf(pFile1, " %s %s %s %d %d %.1f %.1f %.1f %.1f %s %s\n",//文本格式写入.txt文件
					cur->cbaplayer.ID,
					cur->cbaplayer.name,
					cur->cbaplayer.sex,
					cur->cbaplayer.age,
					cur->cbaplayer.number,
					cur->cbaplayer.high,
					cur->cbaplayer.weight,
					cur->cbaplayer.reach,
					cur->cbaplayer.BodyFatPercentage,
					cur->cbaplayer.PEResult,
					cur->cbaplayer.LearingScore);
			fwrite(&(cur->cbaplayer), sizeof(SLTCBANode), 1, pFile);//二进制形式写入.txt文件，文件乱码
			cur = cur->next;
		}
		printf("文件存储成功\n");
	}

	//关闭文件
	fclose(pFile);
	//pFile = NULL;
	system("pause");//暂停
	system("cls");//清屏
}

//加载文件（读取文件信息）
void LoadingCBAPlayer(SLTCBANode** phead) {
	//assert(phead);
	//SLTCBANode* cur = *phead;
	//if (*phead == NULL) {
	//	printf("无文件存储\n");
	//	system("pause");
	//	system("cls");
	//	return;
	//}
	FILE* pFile1 = fopen("D:\\GITTE chuantimu2（数据结构+C++）\\test_2_27(CBA球员注册管理系统 -- C语言)\\CBAPlayer Registration management system2.txt", "r");
	if (pFile1 == NULL) {
		perror("fopen::pFile1\n");
		return;
	}
	//读取文件
	//创建一个新的临时结构体来存储文件中的信息
	Player player;
	while (fscanf(pFile1, " %s %s %s %d %d %f %f %f %f %s %s\n",
		player.ID,
		player.name,
		player.sex,
		&player.age,
		&player.number,
		&player.high,
		&player.weight,
		&player.reach,
		&player.BodyFatPercentage,
		player.PEResult,
		player.LearingScore)!=EOF) {
		//创建一个新结点
		SLTCBANode* newnode = (SLTCBANode*)malloc(sizeof(SLTCBANode));
		newnode->next = NULL;
		/*newnode->cbaplayer.age = player.age;
		newnode->cbaplayer.BodyFatPercentage = player.BodyFatPercentage;
		newnode->cbaplayer.high = player.high;
		newnode->cbaplayer.age = player.age;*/
		//法1：单纯拷贝
		//newnode->cbaplayer = player;
		//法2：内存拷贝
		//memcpy(newnode, &player, sizeof(Player));//将player里面的数据全部都拷贝到新结点newnode中
		memmove(newnode, &player, sizeof(Player));//memmove更加规范
		//头插法
		if (*phead == NULL) {
			*phead = newnode;
		}
		else {
			//找到头结点
			newnode->next = *phead;
			*phead = newnode;
		}
	}
	//关闭文件
	fclose(pFile1);
	printf("文件加载成功\n");
	system("pause");//暂停
	system("cls");//清屏
}

//用户注册
void Register() {
	users ReceiveUserInput, ReadFromFile;//ReceiveUserInput是用来接收用户输入的，ReadFromFile是用来从文件中读取的
	char temp[MAX_NAME] = "";//20个存储空间,用着下面做判断的
	printf("欢迎来到注册界面>\n");
	FILE* pFileRiver = fopen("users.txt", "rb");//二进制只读方式打开文件,rb--只可以读
	fread(&ReadFromFile, sizeof(users), 1, pFileRiver);//将文件里面的信息存储到ReadFromFile中，先读一个试一试
	if (pFileRiver == NULL) {
		perror("fopen::pFileRiver\n");
		return;
	}
	printf("请输入你的账号>");//用户名
	scanf("%s", ReceiveUserInput.id);
	while (1) {//让文件运行起来
		if (strcmp(ReceiveUserInput.id, ReadFromFile.id)) {//用户名不相同
			if (!feof(pFileRiver)) {//未读到文件结尾也可能认为用户名不同
				fread(&ReadFromFile, sizeof(users), 1, pFileRiver);//继续读到文件的结尾

			}
			else {//读到文件末尾了
				break;//跳出文件即可
			}
		}
		else {//用户名相同，重复了
			printf("该ID已被别人征用，请换个ID\n");
			//Sleep(1000);
			fclose(pFileRiver);
			pFileRiver = NULL;
			return;//直接返回
		}
	}

	//创建成功
	printf("请输入您的姓名>");
	scanf("%s", ReceiveUserInput.name);
	printf("请输入您的性别>");
	scanf("%s", ReceiveUserInput.sex);
	printf("请输入您的电话号码>");
	scanf("%s", ReceiveUserInput.phone);
	printf("请输入您的密码>");
	scanf("%s", ReceiveUserInput.password);
	/*int i = 0;
	for (i = 0; i < 30; i++){
		ReceiveUserInput.password[i] = _getch();
		if (ReceiveUserInput.password[i] == '\r'){
			break;
		}
		else
		{
			printf("*");
		}
	}*/
	printf("请确认您的密码>");
	scanf("%s", temp);//进行判断密码是否正确
	/*for (i = 0; i < 30; i++){
		temp[i] = _getch();
		if (temp[i] == '\r'){
			break;
		}
		else{
			printf("*");
		}
	}*/


	//判断
	do {
		if (strcmp(ReceiveUserInput.password, temp) == 0) {//两次密码输入相同
			pFileRiver = fopen("users.txt", "ab");//ab--只可以写
			fwrite(&ReceiveUserInput, sizeof(users), 1, pFileRiver);//将信息写入ReceiveUserInput中
			printf("\n账号注册成功，请登录\n");
			fclose(pFileRiver);
			pFileRiver = NULL;
			return;//直接返回不解释
		}
		else {//两次密码输入不同
			printf("\n两次密码不一致，请重新输入>\n");
			printf("请输入您的密码>");
			scanf("%s", ReceiveUserInput.password);
		    /*for (i = 0; i < 20; i++) {
				ReceiveUserInput.password[i] = _getch();
				if (ReceiveUserInput.password[i] == '\r') {
					break;
				}
				else
				{
					printf("*");
				}
			}
			printf("\n");*/
			printf("请确认您的密码>");
			scanf("%s", temp);//进行判断密码是否正确
			/*for (i = 0; i < 20; i++) {
				temp[i] = _getch();
				if (temp[i] == '\r') {
					break;
				}
				else {
					printf("*");
				}
			}
			printf("\n");*/
		}
	} while (1);
}

//登录用户
int Login() {
	users ReceiveUserInput, ReadFromFile;//ReceiveUserInput是用来接收用户输入的，ReadFromFile是用来从文件中读取的
	FILE* PFile = fopen("users.txt", "rb");//只读文件
	if (!PFile) {
		perror("fopen::PFil\n");
		return;
	}
	printf("欢迎来到登录界面\n");
	fread(&ReadFromFile, sizeof(users), 1, PFile);//先读一个匹配匹配
	printf("请输入账号>");
	scanf("%s", ReceiveUserInput.id);
	while (1) {
		if (strcmp(ReceiveUserInput.id, ReadFromFile.id) == 0) {//匹配成功
			break;
		}
		else {
			if (!feof(PFile)) {//未读到文本的结尾
				fread(&ReadFromFile, sizeof(users), 1, PFile);//往后读
			}
			else {
				printf("此账号不存在，请退出重试\n");
				fclose(PFile);
				PFile = NULL;
				system("pause");//暂停
				system("cls");//清屏
				return 0;
			}
		}
	}

	do {
		printf("请输入密码>");

		int i = 0;
		for (i = 0; i < 20; i++) {
			ReceiveUserInput.password[i] = _getch();
			if (ReceiveUserInput.password[i] == '\r') {
				break;
			}
			else
			{
				printf("*");
			}
		}
		//i = i + 1;
		ReceiveUserInput.password[i++] = '\0';
		printf("\n");

		//scanf("%s", ReceiveUserInput.password);
		if (!strcmp(ReceiveUserInput.password, ReadFromFile.password)) {//密码匹配成功
			printf("\n登录成功，欢迎使用\n");
			printf("\n按任意键继续\n");
			fclose(PFile);
			PFile = NULL;//这个有或无都没什么大影响
			return 1;
		}
		else {
			printf("密码错误，请重新输入\n");
		}
	} while (strcmp(ReceiveUserInput.password, ReadFromFile.password));//输入密码相同即可登录
	return 0;
}


//找回密码
void Reback() {
	users a, b;
	FILE* fp = fopen("users.txt", "r");
	char temp[20];
	int count = 0;
	printf("欢迎来到找回密码界面！\n");
	fread(&b, sizeof(users), 1, fp); 
	printf("请输入账号\n");
	scanf("%s", &a.id);

	while (1){
		if (strcmp(a.id, b.id) == 0){
			break;
		}

		else{
			if (!feof(fp)){
				fread(&b, sizeof(users), 1, fp);
			}

			else{
				printf("查无此人，请重新输入!\n");
				//Sleep(500);
				fclose(fp);
				return;
			}
		}
	}
	printf("请输入姓名：\n");
	scanf("%s", a.name);
	do {
		if (strcmp(a.name, b.name)) {
			printf("姓名输入错误！请重新输入！\n");
			scanf("%s", a.name);
		}
	} while (strcmp(a.name, b.name));

	printf("请输入电话号码：\n");
	scanf("%s", a.phone);
	do {
		if (strcmp(a.phone, b.phone)) {
			printf("电话号码输入错误！请重新输入！\n");
			scanf("%s", a.phone);
		}
	} while (strcmp(a.phone, b.phone));


	printf("请输入性别：\n");
	scanf("%s", a.sex);
	do {
		if (strcmp(a.sex, b.sex)) {
			printf("性别输入错误！请重新输入！\n");
			scanf("%s", a.sex);
		}
	} while (strcmp(a.sex, b.sex));

	printf("您的密码是：%s\n", b.password);
	system("pause");
	system("cls");
}


//创建储存用户账号密码的文件
void Create_File(){
	FILE* pFILE;
	if ((pFILE = fopen("users.txt", "rb")) == NULL){
		if ((pFILE = fopen("users.txt", "wb+")) == NULL){
			printf("无法建立文件！\n");
			exit(0);
		}
	}
}

//登录菜单
void WelcomMenu() {
	printf("\t\t\t|*************************|\t\t\t\n");
	printf("\t\t\t|**欢迎进入注册&登录界面**|\t\t\t\n");
	printf("\t\t\t|*************************|\t\t\t\n");
	printf("\t\t\t|1.注册       | 2.登录    |\t\t\t\n");
	printf("\t\t\t---------------------------\t\t\t\t\n");
	printf("\t\t\t|3.找回密码   | 0.退出    |\t\t\t\n");
	printf("\t\t\t|*************************|\t\t\t\n");
	printf("\n");
	printf("\t\t\t********请输入选项*********\t\t\t\n");

}


//桌面菜单
void WelcomeToMyDesk() {
	printf("\t   ****************************************************\t\t\n");
	printf("\t   ************ 欢迎进入CBA球员注册系统    ************\t\t\n");
	printf("\t   ************ 请按照选项进行选择功能     ************\t\t\n");
	printf("\t   ************ 0.退出      1.增加球员信息 ************\t\t\n");
	printf("\t   ************ 2.打印信息  3.保存         ************\t\t\n");
	printf("\t   ************ 4.加载      5.查找         ************\t\t\n");
	printf("\t   ************ 6.删除      7.指定插入     ************\t\t\n");
	printf("\t   ************ 8.修改信息  9.筛选         ************\t\t\n");
	printf("\t   ************ 10.统计人数 11.排序        ************\t\t\n");
	printf("\t   ************ 12.计算     13.判断        ************\t\t\n");
	printf("\t   ************ 14.清空     15.报表        ************\t\t\n");
	printf("\t   ****************************************************\t\t\n");
	printf("\n");
	printf("\t   ************           请选择           ************\t\t\n");
}

//报表
void Excel(const SLTCBANode* phead) {
	//assert(phead);//空链表可以传过来
	system("cls");
	SLTCBANode* cur = phead;
	//if (cur == NULL) {
	//	printf("以下是报表信息：\n");
	//	printf("---------------------------------------------------------------------------\n");
	//	printf("|\t\t\t欢迎使用CBA球员注册管理系统\t\t\t|\n");
	//	printf("---------------------------------------------------------------------------\n");
	//	printf("|\t身份证号\t\t|\t姓名\t|性别|年龄|\t等级\t|\n");
	//	printf("---------------------------------------------------------------------------\n");
	//}
	printf("以下是报表信息：\n");
	printf("---------------------------------------------------------------------------\n");
	printf("|\t\t\t欢迎使用CBA球员注册管理系统\t\t\t  |\n");
	printf("---------------------------------------------------------------------------\n");
	printf("|\t身份证号\t\t|\t姓名\t|性别|年龄|\t等级\t  |\n");
	printf("---------------------------------------------------------------------------\n");
	while (cur != NULL) {
		printf("|%-31s|%-15s|%-4s|%-4d|%-13s  |\n", 
			cur->cbaplayer.ID, cur->cbaplayer.name, cur->cbaplayer.sex,
			cur->cbaplayer.age, cur->cbaplayer.PEResult);

		cur = cur->next;
	}
	printf("---------------------------------------------------------------------------\n");
	system("pause");
	system("cls");
}