#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

//#define N 1000
//typedef int SLDataType;//改类型方便
//
////静态顺序表：特点：满了就不让插入；缺点：数组给的大小不确定
//typedef struct SeqList 
//{
//	int a[N];
//	int size;//表示数组中存储了多少个数据
//}SL;
//
////接口函数
//void SeqListPushInit(SL* ps); 
//void SeqListPushBack(SL* ps, SLDataType x); 
//void SeqListPopBack(SL* ps); 
//void SeqListPushFront(SL* ps, SLDataType x); 
//void SeqListPopFront(SL* ps);


typedef int SLDataType;

//静态顺序表：特点：满了就不让插入；缺点：数组给的大小不确定
typedef struct SeqList 
{
	SLDataType* a;
	int size;//表示数组中存储了多少个数据
	int capacity;//表示数组的实际能存数据的空间容量是多大
}SL;

//接口函数
//初始化
void SeqListInit(SL* ps); 
//尾插
void SeqListPushBack(SL* ps, SLDataType x); 
//尾删
void SeqListPopBack(SL* ps);
//头插
void SeqListPushFront(SL* ps, SLDataType x); 
//头删
void SeqListPopFront(SL* ps);
//打印
void SeqListPrint(SL* ps);
//销毁空间
void SeqListDestory(SL* ps);
//增容函数
void SeqListCheckCapacity(SL* ps);
//查找位置，找到了返回x位置下标，没有找到返回-1
int SeqListFind(SL* ps, SLDataType x);
//在某一个位置上插入数据
void SeqListInsert(SL* ps, int pos, SLDataType x);
//在指定位置删除指定数据
void SeqListErase(SL* ps, int pos);