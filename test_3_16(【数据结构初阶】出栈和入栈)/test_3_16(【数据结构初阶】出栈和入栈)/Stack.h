#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include<stdbool.h>

typedef int StackDataType;

typedef struct Stack {
	int* a;
	int top; // 栈顶
	int capacity; // 容量
}ST;

//初始化
void StackInit(ST* ps);

//压栈
void StackPush(ST* ps, StackDataType x);

//出栈
void StackPop(ST* ps);

//计算栈中有多少个
int StackSize(ST* ps);

//判断栈是否为空
bool StackEmpty(ST* ps);

//销毁
void StackDestroy(ST* ps);

//栈内的元素
StackDataType StackShow(ST* ps);