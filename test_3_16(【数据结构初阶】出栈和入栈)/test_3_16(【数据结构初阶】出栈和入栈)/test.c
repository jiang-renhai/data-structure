#define _CRT_SECURE_NO_WARNINGS 1

#include"Stack.h"

int main() {
	ST ps;
	StackInit(&ps);
	StackPush(&ps, 1);
	StackPush(&ps, 2);
	StackPush(&ps, 3);
	StackPush(&ps, 4);
	int i = StackShow(&ps);
	printf("%d ", i);
	StackDestroy(&ps);
	return 0;
}