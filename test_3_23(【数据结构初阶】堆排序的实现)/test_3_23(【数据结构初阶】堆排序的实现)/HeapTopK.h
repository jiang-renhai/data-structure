#pragma once


#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<assert.h>
#include<string.h>

// 根部下标从0开始的完全二叉树
// parent = (child - 1)/2
// leftchild = parent * 2 + 1
// rightchild = parent * 2 + 2

typedef int HPDataType;

typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}Heap;

//堆的初始化
void HeapInit(Heap* hp);

//堆的销毁
void HeapDestory(Heap* hp);

//堆的插入
void HeapPush(Heap* hp, HPDataType x);

//堆的删除
void HeapPop(Heap* hp);

//取堆顶的数据
HPDataType HeapTop(Heap* hp);

//堆的数据个数
int HeapSize(Heap* hp);

//堆的判空
bool HeapEmpty(Heap* hp);

//向上调整
void AdjustUp(HPDataType* a, HPDataType child);

//向下调整
void AdjustDown(HPDataType* a, HPDataType n, HPDataType parent);

//交换数据
void Swap(HPDataType* p1, HPDataType* p2);