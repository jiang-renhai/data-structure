#define _CRT_SECURE_NO_WARNINGS 1

#include"HeapTopK.h"

//堆的初始化
void HeapInit(Heap* hp)
{
	assert(hp);
	hp->a = (HPDataType*)malloc(sizeof(HPDataType) * 4);
	if (hp->a == NULL)
	{
		perror("malloc fail");
		return;
	}
	hp->size = 0;
	hp->capacity = 4;
}

//堆的销毁
void HeapDestory(Heap* hp)
{
	assert(hp);
	while (!HeapEmpty(hp))
	{
		hp->size--;
	}
	free(hp->a);
	hp->a = NULL;
	hp->capacity = 0;
}

//交换
void Swap(HPDataType* p1, HPDataType* p2)
{
	HPDataType temp = *p1;
	*p1 = *p2;
	*p2 = temp;

}

//向上调整（除了child，其余全是堆）
void AdjustUp(HPDataType* a, HPDataType child)
{
	//判断孩子和父母的关系
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			//迭代
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

//堆的插入
void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	//判断是否满
	if (hp->capacity == hp->size)
	{
		HPDataType* temp = (HPDataType*)realloc(hp->a, sizeof(HPDataType) * hp->capacity * 2);
		if (temp == NULL)
		{
			perror("realloc fail");
			return;
		}
		hp->a = temp;
		hp->capacity *= 2;
	}
	//插入
	hp->a[hp->size] = x;
	hp->size++;

	AdjustUp(hp->a, hp->size - 1);
}

//向下调整
void AdjustDown(HPDataType* a, HPDataType n, HPDataType parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] > a[child])
		{
			++child;
		}
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

//堆的删除(前提是左右子树都是堆)
void HeapPop(Heap* hp)
{
	assert(hp);
	assert(!HeapEmpty(hp));
	//删除根部
	//先将根部数据与最末尾元素调换一下，再size--
	//最后向下判断
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;

	AdjustDown(hp->a, hp->size, 0);
}

//取堆顶的数据
HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	return hp->a[0];
}

//堆的数据个数
int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}

//堆的判空
bool HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->size == 0;
}