#define _CRT_SECURE_NO_WARNINGS 1

#include"HeapTopK.h"

//堆排序
void HeapSort(int* a, int n)
{
	//插入建堆
	//建堆 -- 向上调整
	//O（N*logN）
	//升序 -- 建大堆
	int i = 1;
	for (i = 1; i < n; i++)
	{
		AdjustUp(a, i);
	}
	//从后往前调整
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		--end;
	}
}


//堆排序
void HeapSort1(int* a, int n)
{
	//建堆 -- 向下调整建堆
	int parent = n - 1;//下标
	int i = 0;
	for (i = (parent - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}
	
	//从后往前调整
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		--end;
	}
}

int main()
{
	int a[10] = { 2,1,5,7,6,8,0,9,4,3 };
	//HeapSort(a, 10);
	HeapSort1(a, 10);
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
	return 0;
}