#define _CRT_SECURE_NO_WARNINGS 1

#include"Queue.h"

int main() {
	Queue pq;
	QueueInit(&pq);
	QueuePush(&pq, 1);
	QueuePush(&pq, 2);
	QueuePush(&pq, 3);
	QueuePush(&pq, 4);
	QueuePush(&pq, 5);
	
	//只能先进再出进的值，不能遍历
	while (!QueueEmpty(&pq)) {
		printf("%d ", QueueFront(&pq));
		QueuePop(&pq);
	}
	printf("\n");

	int i = QueueSize(&pq);
	printf("%d", i);

	QueueDestroy(&pq);
	return 0;
}