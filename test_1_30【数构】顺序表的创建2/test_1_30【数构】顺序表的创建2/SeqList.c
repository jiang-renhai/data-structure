#define _CRT_SECURE_NO_WARNINGS 1

#include"SeqList.h"

//打印
void SeqListPrint(SL* ps) {
	for (int i = 0; i < ps->size; i++) {
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

//初始化
void SeqListInit(SL* ps) {
	ps->a = NULL;
	ps->size = ps->capacity = 0;

}

//销毁空间
void SeqListDestory(SL* ps) {
	free(ps->a);
	ps->a = NULL;
	ps->capacity = ps->size = 0;
}

//增容
void SeqListCheckCapacity(SL* ps) {
	//容量已经到了顶或者都为0
	if (ps->size == ps->capacity) {
		int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		SLDataType* tmp = (SLDataType*)realloc(ps->a, newcapacity * sizeof(SLDataType));
		//没有空间就扩容失败
		if (tmp == NULL) {
			printf("realloc error\n");
			exit(-1);
		}
		//扩容成功，扩容成功的空间给数组
		ps->a = tmp;
		ps->capacity = newcapacity;
	}
}

//尾插
void SeqListPushBack(SL* ps, SLDataType x) {
	////分装函数--增容
	////容量已经到了顶或者都为0
	//if (ps->size == ps->capacity) {
	//	int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
	//	SLDataType* tmp = (SLDataType*)realloc(ps->a, newcapacity * sizeof(SLDataType));
	//	//没有空间就扩容失败
	//	if (tmp == NULL) {
	//		printf("realloc error\n");
	//		exit(-1);
	//	}
	//	//扩容成功，扩容成功的空间给数组
	//	ps->a = tmp;
	//	ps->capacity = newcapacity;
	//}
	//SeqListCheckCapacity(ps);
	//ps->a[ps->size] = x;
	//ps->size++;
	SeqListInsert(ps, ps->size, x);
}

//尾删
void SeqListPopBack(SL* ps) {
	//////温柔方式
	////if (ps->size > 0) {
	////	ps->size--;
	////}
	////暴力方式
	//assert(ps->size > 0);
	//ps->size--;
	SeqListErase(ps, ps->size - 1);
}

//头插
void SeqListPushFront(SL* ps, SLDataType x) {
	//SeqListCheckCapacity(ps);
	//////容量已经到了顶或者都为0
	////if (ps->size == ps->capacity) {
	////	int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
	////	SLDataType* tmp = (SLDataType*)realloc(ps->a, newcapacity * sizeof(SLDataType));
	////	//没有空间就扩容失败
	////	if (tmp == NULL) {
	////		printf("realloc error\n");
	////		exit(-1);
	////	}
	////	//扩容成功，扩容成功的空间给数组
	////	ps->a = tmp;
	////	ps->capacity = newcapacity;
	////}
	////挪动数据
	//int end = ps->size - 1;
	//while (end >= 0) {
	//	ps->a[end + 1] = ps->a[end];
	//	end--;
	//}
	//ps->a[0] = x;
	//ps->size++;
	SeqListInsert(ps, 0, x);
}


//头删
void SeqListPopFront(SL* ps) {
	//assert(ps->size > 0);
	////挪动数据
	//int begin = 1;
	//while (begin < ps->size) {
	//	ps->a[begin - 1] = ps->a[begin];
	//	begin++;
	//}
	//ps->size--;
	SeqListErase(ps, 0);
}


//查找
int SeqListFind(SL* ps, SLDataType x) {
	for (int i = 0; i < ps->size; i++) {
		if (ps->a[i] == x) {
			return i;
		}
	}
	return -1;
}

//在某一个位置上插入数据
void SeqListInsert(SL* ps, int pos, SLDataType x) {
	assert(pos <= ps->size && pos >= 0);
	SeqListCheckCapacity(ps);
	//挪动数据
	int end = ps->size - 1;
	while (end >= pos) {
		ps->a[end + 1] = ps->a[end];
		end--;
	}
	ps->a[pos] = x;
	ps->size++;
}

//在指定位置删除指定数据
void SeqListErase(SL* ps, int pos) {
	assert(pos < ps->size&& pos >= 0);
	int begin = pos + 1;
	while (begin < ps->size) {
		ps->a[begin - 1] = ps->a[begin];
		begin++;
	}
	ps->size--;

}

//排序
void SeqListSortName(void* elem1, void* elem2) {
	return *(SLDataType*)elem1 - *(SLDataType*)elem2;
}

//清空
void SeqListEmpty(SL* ps) {
	ps->size = ps->capacity = 0;
	ps->a = NULL;
}