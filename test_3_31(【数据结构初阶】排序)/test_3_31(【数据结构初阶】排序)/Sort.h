#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

// 插入排序
void InsertSort(int* a, int n);

//打印
void PrintSort(int* a, int n);

//希尔排序
void ShellSort(int* a, int n);

//希尔排序
void ShellSort1(int* a, int n);

//选择排序
void SelectSort(int* a, int n);

//堆排序
void HeapSort1(int* a, int n);

//冒泡排序
void BubbleSort(int* a, int n);

//快速排序 -- 初始版本
void QuickSorts(int* a, int left, int right);

//快速排序 -- 优化（随机取key和三数取中key）
void QuickSort1(int* a, int left, int right);

//三数取中
int GetMidNumi(int* a, int left, int right);

//快速排序 -- 挖坑法
void QuickSort2(int* a, int left, int right);

//快速排序 -- 有返回值的
void QuickSort(int* a, int left, int right);

//快速排序 -- 前后指针
void QuickSort3(int* a, int left, int right);

//初始版本
int partsort1(int* a, int left, int right);

//优化版本
int partsort2(int* a, int left, int right);

//挖坑法
int partsort3(int* a, int left, int right);

//前后指针法
int partsort4(int* a, int left, int right);

//快速排序 -- 非递归版本
void QuickSortStack(int* a, int left, int right);

//带返回值的快速排序 -- 优化版
void QuickSortp(int* a, int left, int right);