#define _CRT_SECURE_NO_WARNINGS 1

#include"Stack.h"

//初始化
void StackInit(ST* ps) {
	assert(ps);
	//先开辟4个空间
	ps->a = (StackDataType*)malloc(sizeof(StackDataType) * 4);
	if (ps->a == NULL) {
		perror("malloc::ps->a");
		return;
	}
	ps->capacity = 4;
	ps->top = 0; //栈顶后一个位置

	//ps->top = -1; //栈顶本身的位置
}

//压栈
void StackPush(ST* ps, StackDataType x) {
	assert(ps);

	// 判断是否需要扩容
	if (ps->capacity == ps->top) {
		//扩容
		StackDataType* temp = (StackDataType*)realloc(ps->a, sizeof(StackDataType) * ps->capacity * 2);
		if (temp == NULL) {
			perror("malloc failed");
			return;
		}
		ps->a = temp;
		ps->capacity *= 2;
	}
	
	ps->a[ps->top] = x;
	ps->top++;
}

//出栈
void StackPop(ST* ps) {
	assert(ps);
	assert(!(StackEmpty(ps)));
	ps->top--;
}

//计算栈中有多少个
int StackSize(ST* ps) {
	assert(ps);

	return ps->top;
}

//判断栈是否为空
bool StackEmpty(ST* ps) {
	assert(ps);

	return ps->top == 0;//是空返回false，不是空返回true
}

//销毁
void StackDestroy(ST* ps) {
	assert(ps);
	free(ps->a);
	ps->a = NULL;
	ps->capacity = 0;
	ps->top = 0;
}

//栈顶的元素
StackDataType StackShow(ST* ps) {
	assert(ps);
	return ps->a[ps->top - 1];
}