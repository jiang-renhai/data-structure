#define _CRT_SECURE_NO_WARNINGS 1

#include"Sort.h"
#include"Stack.h"

//插入排序
//升序
void InsertSort(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		//先写一趟
		int end = i - 1;
		int tmp = a[i];
		while (end >= 0)
		{
			if (tmp < a[end])
			{
				//数据往后移动
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}

//打印
void PrintSort(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

//希尔排序
void ShellSort1(int* a, int n)
{
	//升序
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int j = 0; j < gap; j++)
		{
			for (int i = j; i < n; i += gap)
			{
				int end = i - gap;
				int tmp = a[end + gap];//存的是值

				while (end >= 0)
				{
					if (tmp < a[end])
					{
						a[end + gap] = a[end];
						end = end - gap;
					}
					else
					{
						break;
					}
				}
				a[end + gap] = tmp;
			}
		}
	}
}


//希尔排序
void ShellSort(int* a, int n)
{
	//int gap = 3;
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		//gap /= 2;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[i + gap];
			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

//交换数据
void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

//选择排序，两端向中间排序
//升序
void SelectSort(int* a, int n)
{
	int left = 0;
	int right = n - 1;
	
	while (left < right)
	{
		int mini = left;
		int maxi = left;
		for (int i = left + 1; i <= right; i++)
		{
			if (a[i] < a[mini])
			{
				mini = i;
			}
			if (a[i] > a[maxi])
			{
				maxi = i;
			}
		}
		Swap(&a[mini], &a[left]);
		//如果最大值和最小值在left或者right时候，更新一下maxi的地方
		if (maxi == left)
		{
			maxi = mini;
		}
		Swap(&a[maxi], &a[right]);
		left++;
		right--;
	}
}

//向下调整
//时间复杂度：O(N)
void AdjustDown(int* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] > a[child])
		{
			++child;
		}
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

//堆排序
void HeapSort1(int* a, int n)
{
	//建堆 -- 向下调整建堆 - 时间复杂度为O(N)
	int parent = n - 1;//下标
	int i = 0;
	for (i = (parent - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}

	//从后往前调整
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		--end;
	}
}

//冒泡排序
void BubbleSort(int* a, int n)
{
	for (int j = 0; j < n; j++)
	{
		int flag = 0;
		//内层
		for (int i = 1; i < n - j; i++)
		{
			if (a[i - 1] > a[i])
			{
				Swap(&a[i - 1], &a[i]);
				flag = 1;
			}
		}
		if (flag == 0)
		{
			break;
		}
	}
}

//三数取中
int GetMidNumi(int* a, int left, int right)
{
	int mid = (left + right) / 2;
	if (a[left] < a[mid])
	{
		if (a[mid] < a[right])//mid最小
		{
			return mid;
		}
		else if (a[left] > a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else //a[left] >= a[mid]
	{
		if (a[mid] > a[right])
		{
			return mid;
		}
		else if (a[right] > a[left])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}

//快速排序 - 优化版本
void QuickSort1(int* a, int left, int right)
{
	if (left >= right)
		return;
	int begin = left;
	int end = right;

	//优化
	//随机值法
	//int randomi = left + (rand() % (right - left));
	//Swap(&a[left], &a[randomi]);
	//三数取中
	int middle = GetMidNumi(a, left, right);
	Swap(&a[left], &a[middle]);

	int key = left;
	while (left < right)
	{
		while (left < right && a[right] >= a[key])
			--right;
		while (left < right && a[left] <= a[key])
			++left;
		Swap(&a[right], &a[left]);
	}
	Swap(&a[key], &a[left]);

	key = left;
	QuickSort(a, begin, key - 1);
	QuickSort(a, key + 1, end);
}

//快速排序 - 初始版本
void QuickSorts(int* a, int left, int right)
{
	if (left >= right)
		return;
	int key = left;
	int begin = left;
	int end = right;
	while (left < right)
	{
		while (left < right && a[right] >= a[key])
			--right;
		while (left < right && a[left] <= a[key])
			++left;
		Swap(&a[left], &a[right]);
	}
	Swap(&a[left], &a[key]);
	key = left;

	QuickSort1(a, begin, key - 1);
	QuickSort1(a, key + 1, end);
}

//快速排序 -- 挖坑法
void QuickSort2(int* a, int left, int right)
{
	if (left >= right)
		return;
	int begin = left;
	int end = right;
	//三数取中
	int middle = GetMidNumi(a, left, right);
	Swap(&a[left], &a[middle]);

	int key = a[left];
	int hole = left;
	while (left < right)
	{
		while (left < right && a[right] >= key)
			--right;
		a[hole] = a[right];
		hole = right;
		while (left < right && a[left] <= key)
			++left;
		a[hole] = a[left];
		hole = left;
	}
	a[hole] = key;

	QuickSort2(a, begin, hole - 1);
	QuickSort2(a, hole + 1, end);
}

//快速排序 -- 前后指针
void QuickSort3(int* a, int left, int right)
{
	if (left >= right)
		return;

	int begin = left;
	int end = right;
	//三数取中
	int middle = GetMidNumi(a, left, right);
	Swap(&a[left], &a[middle]);

	int key = left;
	int prev = left;
	int cur = left + 1;
	while (cur <= right)
	{
		if (a[cur] < a[key])
		{
			++prev;
			Swap(&a[cur], &a[prev]);
			++cur;
		}
		else
		{
			++cur;
		}
	}
	Swap(&a[prev], &a[key]);
	key = prev;

	QuickSort3(a, begin, key - 1);
	QuickSort3(a, key + 1, end);

}

//初始版本
int partsort1(int* a, int left, int right)
{
	int key = left;
	int begin = left;
	int end = right;
	while (left < right)
	{
		while (left < right && a[right] >= a[key])
			--right;
		while (left < right && a[left] <= a[key])
			++left;
		Swap(&a[left], &a[right]);
	}
	Swap(&a[left], &a[key]);
	key = left;

	return key;
}

//优化版本
int partsort2(int* a, int left, int right)
{
	int begin = left;
	int end = right;

	//优化
	//随机值法
	//int randomi = left + (rand() % (right - left));
	//Swap(&a[left], &a[randomi]);
	//三数取中
	int middle = GetMidNumi(a, left, right);
	Swap(&a[left], &a[middle]);

	int key = left;
	while (left < right)
	{
		while (left < right && a[right] >= a[key])
			--right;
		while (left < right && a[left] <= a[key])
			++left;
		Swap(&a[right], &a[left]);
	}
	Swap(&a[key], &a[left]);

	key = left;

	return key;
}

//挖坑法
int partsort3(int* a, int left, int right)
{
	int begin = left;
	int end = right;
	//三数取中
	int middle = GetMidNumi(a, left, right);
	Swap(&a[left], &a[middle]);

	int key = a[left];
	int hole = left;
	while (left < right)
	{
		while (left < right && a[right] >= key)
			--right;
		a[hole] = a[right];
		hole = right;
		while (left < right && a[left] <= key)
			++left;
		a[hole] = a[left];
		hole = left;
	}
	a[hole] = key;

	return hole;
}

//前后指针法
int partsort4(int* a, int left, int right)
{
	int begin = left;
	int end = right;
	//三数取中
	int middle = GetMidNumi(a, left, right);
	Swap(&a[left], &a[middle]);

	int key = left;
	int prev = left;
	int cur = left + 1;
	while (cur <= right)
	{
		if (a[cur] < a[key])
		{
			++prev;
			Swap(&a[cur], &a[prev]);
			++cur;
		}
		else
		{
			++cur;
		}
	}
	Swap(&a[prev], &a[key]);
	key = prev;

	return key;
}

//带返回值的快速排序
void QuickSort(int* a, int left, int right)
{
	if (left >= right)
	{
		return;
	}
	int keyi = partsort4(a, left, right);
	QuickSort(a, left, keyi - 1);
	QuickSort(a, keyi + 1, right);
}


//带返回值的快速排序 -- 优化版
void QuickSortp(int* a, int left, int right)
{
	if (left >= right)
	{
		return;
	}
	//大区间使用快速排序
	if (right - left + 1 > 10)
	{
		int keyi = partsort4(a, left, right);
		QuickSort(a, left, keyi - 1);
		QuickSort(a, keyi + 1, right);
	}
	//小区间直接使用插入排序
	else
	{
		InsertSort(a + left, right - left + 1);
	}
}

//快速排序 -- 非递归版本
void QuickSortStack(int* a, int left, int right)
{
	ST st;
	StackInit(&st);
	//先入右再入左，出栈的时候是先出左再出右
	StackPush(&st, right);
	StackPush(&st, left);

	while (!StackEmpty(&st))
	{
		//取头，再取尾
		int begin = StackShow(&st);
		StackPop(&st);
		int end = StackShow(&st);
		StackPop(&st);

		//找到keyi的位置
		int keyi = partsort4(a, begin, end);
		//[begin, keyi-1] keyi [keyi+1, end]
		//先入右再左
		if (keyi + 1 < end)//中间的数大于等于两个数
		{
			//先右再左
			StackPush(&st, end);
			StackPush(&st, keyi + 1);
		}

		if (begin < keyi - 1)
		{
			//先右再左
			StackPush(&st, keyi - 1);
			StackPush(&st, begin);
		}
	}

	StackDestroy(&st);
}
