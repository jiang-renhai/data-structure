#define _CRT_SECURE_NO_WARNINGS 1

//递归
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int BTDataType;
typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;


BTNode* BuyNode(BTDataType x)
{
	BTNode* BTnode = (BTNode*)malloc(sizeof(BTNode));
	if (BTnode == NULL)
	{
		perror("malloc fail");
		return;
	}

	BTnode->data = x;
	BTnode->left = NULL;
	BTnode->right = NULL;

	return BTnode;
}

BTNode* CreatBinaryTree()
{
	BTNode* node1 = BuyNode(1);
	BTNode* node2 = BuyNode(2);
	BTNode* node3 = BuyNode(3);
	BTNode* node4 = BuyNode(4);
	BTNode* node5 = BuyNode(5);
	BTNode* node6 = BuyNode(6);
	//BTNode* node7 = BuyNode(7);

	node1->left = node2;
	node1->right = node4;

	node2->left = node3;

	node4->left = node5;
	node4->right = node6;

	//node3->right = node7;

	return node1;
}

//前序 -- 根、左子树、右子树
void PreOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	printf("%d ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}

//中序 -- 左子树、根、右子树
void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

//后序 -- 左子树、右子树、根
void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

//算结点个数 -- 指挥打工人统计数量报上来
int TreeSize(BTNode* root)
{
	//指挥打工人
	return root == NULL ? 0 : 
			TreeSize(root->left)
			+ TreeSize(root->right) 
			+ 1;
}

//当前树的高度 == 左右子树高的那个层数+1
int TreeHeight(BTNode* root)
{
	if (root == NULL)
		return 0;
	int LeftTreeHeight = TreeHeight(root->left);
	int RightTreeHeight = TreeHeight(root->right);

	return LeftTreeHeight > RightTreeHeight ? 
			LeftTreeHeight + 1 
			: RightTreeHeight + 1;
}

//二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	assert(k > 0);
	//当前树的第k层的个数 = 左子树的第k-1层个数 + 左子树的第k-1层个数
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;
	int LeftK = BinaryTreeLevelKSize(root->left, k - 1);
	int RightK = BinaryTreeLevelKSize(root->right, k - 1);
	return LeftK + RightK;
}

int main()
{
	BTNode* root = CreatBinaryTree();
	PreOrder(root);
	printf("\n");
	InOrder(root);
	printf("\n");
	PostOrder(root);
	printf("\n");

	int size = TreeSize(root);
	printf("TreeSize:%d\n", size);
	int size1 = TreeSize(root);
	printf("TreeSize:%d\n", size1);

	int height = TreeHeight(root);
	printf("HeightSize:%d\n", height);

	int K1 = BinaryTreeLevelKSize(root, 3);//层数
	printf("BinaryTreeLevelKSize:%d\n", K1);

	int K2 = BinaryTreeLevelKSize(root, 2);//层数
	printf("BinaryTreeLevelKSize:%d\n", K2);
	return 0;
}