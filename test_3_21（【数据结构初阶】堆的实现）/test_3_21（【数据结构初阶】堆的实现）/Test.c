#define _CRT_SECURE_NO_WARNINGS 1
#include"Heap.h"

int main() 
{
	Heap hp;
	HeapInit(&hp);
	HeapPush(&hp, 1);
	HeapPush(&hp, 31);
	HeapPush(&hp, 21);
	HeapPush(&hp, 24);
	HeapPush(&hp, 4);
	HeapPush(&hp, 76);
	HeapPush(&hp, 38);
	HeapPush(&hp, 37);
	HeapPush(&hp, 30);
	HeapPush(&hp, 37);
	HeapPush(&hp, 37);

	while (!HeapEmpty(&hp))
	{
		printf("%d ", HeapTop(&hp));
		HeapPop(&hp);
	}
	printf("\n");

	HeapDestory(&hp);

	return 0;
}